<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index')->name('admin');
Route::prefix('user')->group(function () {
    Route::get('/', 'User\UserController@index');
    Route::resource('profile', 'User\ProfileController');
    Route::get('profile/{id}/password', 'User\ProfileController@password');
    Route::patch('profile/{id}/password/update', 'User\ProfileController@updatePassword');
    Route::resource('permission', 'User\PermissionController');
    Route::resource('role', 'User\RoleController');
    Route::resource('member', 'User\MemberController');
});
Route::resource('customer', 'CustomerController');
Route::resource('supplier', 'SupplierController');

Route::resource('product', 'ProductController');
Route::post('product/{product}', 'ProductController@storeVariant');
Route::get('product/{product}/variant/{variant}/edit', 'ProductController@editVariant');
Route::patch('product/{product}/variant/{variant}', 'ProductController@updateVariant');
Route::delete('product/{product}/variant/{variant}', 'ProductController@destroyVariant');

Route::post('upload', 'HomeController@upload');

Route::resource('order', 'OrderController');
Route::post('order/{order}', 'OrderController@storeOrderLineItem');
Route::delete('order/{order}/item/{item}', 'OrderController@destroyOrderLineItem');

Route::get('order/{order}/out/create', 'OrderController@createOrderOut');
Route::get('order/{order}/out', 'OrderController@getOrderOut');
Route::post('order/{order}/out', 'OrderController@storeOrderOut');
Route::get('order/{order}/out/edit', 'OrderController@editOrderOut');
Route::patch('order/{order}/out/{out}', 'OrderController@updateOrderOut');
Route::post('order/{order}/out/{out}', 'OrderController@storeOrderOutLineItem');
Route::delete('order/{order}/out/{out}/item/{item}', 'OrderController@destroyOrderOutLineItem');

Route::resource('delivery', 'DeliveryOrderController');
Route::post('delivery/{delivery}', 'DeliveryOrderController@storeDeliveryLineItem');
Route::delete('delivery/{delivery}/item/{item}', 'DeliveryOrderController@destroyDeliveryLineItem');

Route::resource('invoice', 'InvoiceController');
Route::resource('cash', 'CashController');

Route::prefix('report')->group(function () {
    Route::get('/', 'ReportController@index');
    Route::get('/{type}', 'ReportController@getReport');
});

Auth::routes();
