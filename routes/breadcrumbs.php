<?php

/* Home */
Breadcrumbs::register('home', function ($trail) {
	$trail->push('Beranda', url('/'));
});

Breadcrumbs::register('user', function ($trail) {
	$trail->parent('home');
	$trail->push('Manajemen Pengguna', url('user'));
});

Breadcrumbs::register('customer', function ($trail) {
	$trail->parent('home');
	$trail->push('Customer', url('customer'));
});

Breadcrumbs::register('supplier', function ($trail) {
	$trail->parent('home');
	$trail->push('Supplier', url('supplier'));
});

Breadcrumbs::register('product', function ($trail) {
	$trail->parent('home');
	$trail->push('Produk', url('product'));
});

Breadcrumbs::register('order', function ($trail) {
	$trail->parent('home');
	$trail->push('Order', url('order'));
});

Breadcrumbs::register('delivery', function ($trail) {
	$trail->parent('home');
	$trail->push('Surat Jalan', url('delivery'));
});

Breadcrumbs::register('invoice', function ($trail) {
	$trail->parent('home');
	$trail->push('Invoice', url('invoice'));
});

Breadcrumbs::register('cash', function ($trail) {
	$trail->parent('home');
	$trail->push('Kas', url('cash'));
});

Breadcrumbs::register('report', function ($trail) {
	$trail->parent('home');
	$trail->push('Laporan', url('report'));
});
/* Home */

/* User Management */
Breadcrumbs::register('profile', function ($trail) {
	$trail->parent('user');
	$trail->push('Profil', url('user/profile'));
});

Breadcrumbs::register('profile-edit', function ($trail, $item) {
	$trail->parent('profile');
	$trail->push('Ubah', url('user/profile/' . $item->id . '/edit'));
});

Breadcrumbs::register('profile-password', function ($trail, $item) {
	$trail->parent('profile');
	$trail->push('Password', url('user/profile/' . $item->id . '/password'));
});

Breadcrumbs::register('permission', function ($trail) {
	$trail->parent('user');
	$trail->push('Permission', url('user/permission'));
});

Breadcrumbs::register('permission-edit', function ($trail, $item) {
	$trail->parent('permission');
	$trail->push('Ubah', url('user/permission/' . $item->id . '/edit'));
});

Breadcrumbs::register('role', function ($trail) {
	$trail->parent('user');
	$trail->push('Role', url('user/role'));
});

Breadcrumbs::register('role-edit', function ($trail, $item) {
	$trail->parent('role');
	$trail->push('Ubah', url('user/role/' . $item->id . '/edit'));
});

Breadcrumbs::register('member', function ($trail) {
	$trail->parent('user');
	$trail->push('Member', url('user/member'));
});

Breadcrumbs::register('member-edit', function ($trail, $item) {
	$trail->parent('member');
	$trail->push('Ubah', url('user/member/' . $item->id . '/edit'));
});
/* User Management */

/* Customer */
Breadcrumbs::register('customer-create', function ($trail) {
	$trail->parent('customer');
	$trail->push('Tambah', url('customer/create'));
});

Breadcrumbs::register('customer-edit', function ($trail, $item) {
	$trail->parent('customer');
	$trail->push('Ubah', url('customer/' . $item->id . '/edit'));
});
/* Customer */

/* Supplier */
Breadcrumbs::register('supplier-create', function ($trail) {
	$trail->parent('supplier');
	$trail->push('Tambah', url('supplier/create'));
});

Breadcrumbs::register('supplier-edit', function ($trail, $item) {
	$trail->parent('supplier');
	$trail->push('Ubah', url('supplier/' . $item->id . '/edit'));
});
/* Supplier */

/* Product */
Breadcrumbs::register('product-edit', function ($trail, $item) {
	$trail->parent('product');
	$trail->push('Ubah', url('product/' . $item->id . '/edit'));
});

Breadcrumbs::register('variant', function ($trail, $item) {
	$trail->parent('product');
	$trail->push('Varian', url('product/' . $item->id));
});

Breadcrumbs::register('variant-edit', function ($trail, $product, $variant) {
	$trail->parent('variant', $product);
	$trail->push('Ubah', url('product/' . $product->id . '/variant/' . $variant->id . '/edit'));
});
/* Product */

/* Order */
Breadcrumbs::register('order-create', function ($trail) {
	$trail->parent('order');
	$trail->push('Tambah', url('order/create'));
});

Breadcrumbs::register('order-detail', function ($trail, $item) {
	$trail->parent('order');
	$trail->push('Detil', url('order/' . $item->id));
});

Breadcrumbs::register('order-edit', function ($trail, $item) {
	$trail->parent('order-detail', $item);
	$trail->push('Ubah', url('order/' . $item->id . '/edit'));
});

Breadcrumbs::register('order-out', function ($trail, $item) {
	$trail->parent('order-detail', $item);
	$trail->push('PO Keluar', url('order/' . $item->id . '/out'));
});

Breadcrumbs::register('order-out-create', function ($trail, $item) {
	$trail->parent('order-detail', $item);
	$trail->push('Tambah', url('order/' . $item->id . '/out/create'));
});

Breadcrumbs::register('order-out-edit', function ($trail, $item) {
	$trail->parent('order-out', $item);
	$trail->push('Ubah', url('order/' . $item->id . '/out/edit'));
});
/* Order */

/* Delivery Order */
Breadcrumbs::register('delivery-create', function ($trail) {
	$trail->parent('delivery');
	$trail->push('Tambah', url('delivery/create'));
});

Breadcrumbs::register('delivery-detail', function ($trail, $item) {
	$trail->parent('delivery');
	$trail->push('Detil', url('delivery/' . $item->id));
});

Breadcrumbs::register('delivery-edit', function ($trail, $item) {
	$trail->parent('delivery-detail', $item);
	$trail->push('Ubah', url('delivery/' . $item->id . '/edit'));
});
/* Delivery Order */

/* Invoice */
Breadcrumbs::register('invoice-create', function ($trail) {
	$trail->parent('invoice');
	$trail->push('Tambah', url('invoice/create'));
});

Breadcrumbs::register('invoice-detail', function ($trail, $item) {
	$trail->parent('invoice');
	$trail->push('Detil', url('invoice/' . $item->id));
});

Breadcrumbs::register('invoice-edit', function ($trail, $item) {
	$trail->parent('invoice-detail', $item);
	$trail->push('Ubah', url('invoice/' . $item->id . '/edit'));
});
/* Invoice */

/* Cash */
Breadcrumbs::register('cash-edit', function ($trail) {
	$trail->parent('cash');
	$trail->push('Ubah Kas', url('cash/'));
});
/* Cash */

/* Report */
Breadcrumbs::register('report-order-in', function ($trail) {
	$trail->parent('report');
	$trail->push('Laporan Order Masuk', url('report/order-in'));
});

Breadcrumbs::register('report-order-out', function ($trail) {
	$trail->parent('report');
	$trail->push('Laporan Order Keluar', url('report/order-out'));
});

Breadcrumbs::register('report-delivery', function ($trail) {
	$trail->parent('report');
	$trail->push('Surat Jalan', url('report/delivery'));
});

Breadcrumbs::register('report-cash', function ($trail) {
	$trail->parent('report');
	$trail->push('Kas', url('report/cash'));
});

Breadcrumbs::register('report-profit', function ($trail) {
	$trail->parent('report');
	$trail->push('Keuntungan', url('report/profit'));
});
/* Report */
