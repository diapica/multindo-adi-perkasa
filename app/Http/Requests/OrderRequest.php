<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer' => 'nullable|int',
            'supplier' => 'nullable|int',
            'order_number' => 'required|string|max:255',
            'order_date' => 'required|date',
            'discount' => 'nullable|int',
            'vat' => 'nullable|int|max:100',
            'tax' => 'nullable|int',
            'shipping_date' => 'nullable|date',
            'address' => 'required|string',
            'status' => 'nullable|string',
        ];
    }
}
