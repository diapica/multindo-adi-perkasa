<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\Permission\Models\Permission;

class PermissionRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$permission = Permission::where('name', $this->name)->first();
		$permissionID = null;
		if ($permission) {
			$permissionID = $permission->id;
		}

		switch ($this->method()) {
			case 'GET':
			case 'DELETE': {
					return [];
				}
			case 'POST': {
					return [
						'name' => 'required|string|max:255|unique:permissions',
					];
				}
			case 'PUT': {
					return [];
				}
			case 'PATCH': {
					return [
						'name' => 'required|string|max:255|unique:permissions,name,' . $permissionID,
					];
				}
			default:
				break;
		}
	}
}
