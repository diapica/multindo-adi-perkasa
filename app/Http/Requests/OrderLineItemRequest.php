<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OrderLineItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qty' => 'required|int',
            'unit' => 'required|string',
            'price' => 'required|int',
            'product' => 'required|string',
            'variant' => 'nullable|string',
            'description' => 'nullable|string|max:255',
            'image' => 'nullable|image'
        ];
    }
}
