<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $role = Role::where('name', $this->name)->first();
        $roleID = null;
        if ($role) {
            $roleID = $role->id;
        }

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {
                    return [
                        'name' => 'required|string|max:255|unique:roles',
                    ];
                }
            case 'PUT': {
                    return [];
                }
            case 'PATCH': {
                    return [
                        'name' => 'required|string|max:255|unique:roles,name,' . $roleID,
                    ];
                }
            default:
                break;
        }
    }
}
