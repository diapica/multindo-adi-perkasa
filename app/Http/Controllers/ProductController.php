<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Product;
use App\Http\Models\Variant;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\VariantRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage products'
        ]);
    }

    public function index(Request $request)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = new Product();

        // Setup columns in table (manual)
        $columns = [
            'name',
        ];

        $tablehandler = new TableHandler();
        $items = $tablehandler->search($items, $columns, $search);
        $items = $tablehandler->sort($items, $sort);
        $items = $tablehandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        return view('pages.product.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
    }

    public function store(ProductRequest $request)
    {
        $data = new Product();
        $data->name = strtoupper($request->name);
        $data->created_by = Auth::user()->id;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect('product/' . $data->id)
            ->with('sucess', 'Produk ' . ucwords(strtolower($request->name)) . ' berhasil dibuat.');
    }

    public function show(Request $request, $productID)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = Variant::where('product_id', $productID);

        // Setup columns in table (manual)
        $columns = [
            'name',
        ];

        $tableHandler = new TableHandler();
        $items = $tableHandler->search($items, $columns, $search);
        $items = $tableHandler->sort($items, $sort);
        $items = $tableHandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        $product = Product::where('id', $productID)->first();

        return view('pages.product.variant.index', compact('items', 'iterator', 'page', 'product', 'search', 'show', 'sort', 'token'));
    }

    public function edit(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();

        return view('pages.product.edit', compact('product'));
    }

    public function update(ProductRequest $request, $id)
    {
        $data = [
            'name' => strtoupper($request->name),
            'updated_by' => Auth::user()->id,
        ];
        Product::where('id', $id)->update($data);

        return redirect('product')
            ->with('success', 'Produk ' . ucwords(strtolower($request->name)) . ' berhasil diubah.');
    }

    public function destroy($id)
    {
        $variant = Variant::where('product_id', $id)->first();
        if (!$variant) {
            try {
                DB::beginTransaction();

                Product::where('id', $id)->delete();
                DB::table('products')->where('id', $id)
                    ->update(['deleted_by' => Auth::User()->id]);

                DB::commit();

                return redirect('product')
                    ->with('success', 'Produk berhasil dihapus');
            } catch (\Exception $e) {
                DB::rollback();

                return redirect('product')
                    ->with('failed', $e->getMessage());
            }
        }
        return redirect('product')
            ->with('failed', 'Produk tidak dapat dihapus karena masih digunakan oleh data lain.');
    }

    public function storeVariant(VariantRequest $request, $productID)
    {
        $data = new Variant();
        $data->product_id = $productID;
        $data->name = strtoupper($request->name);
        $data->created_by = Auth::user()->id;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect('product/' . $productID)
            ->with('success', 'Varian ' . ucwords(strtolower($request->name)) . ' berhasil dibuat.');
    }

    public function editVariant($productID, $variantID)
    {
        $product = Product::where('id', $productID)->first();
        $variant = Variant::where('id', $variantID)->first();

        return view('pages.product.variant.edit', compact('product', 'variant'));
    }

    public function updateVariant(VariantRequest $request, $productID, $variantID)
    {
        $data = [
            'name' => strtoupper($request->name),
            'updated_by' => Auth::user()->id,
        ];
        Variant::where('id', $variantID)->update($data);

        return redirect('product/' . $productID)
            ->with('success', 'Varian ' . ucwords(strtolower($request->name)) . ' berhasil diubah.');
    }

    public function destroyVariant($productID, $variantID)
    {
        try {
            DB::beginTransaction();

            Variant::where('id', $variantID)->delete();
            DB::table('variants')->where('id', $variantID)
                ->update(['deleted_by' => Auth::user()->id]);

            DB::commit();

            return redirect('product/' . $productID)
                ->with('success', 'Varian berhasil dihapus.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('product/' . $productID)
                ->with('failed', $e->getMessage());
        }
    }
}
