<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\Order;
use App\Http\Models\OrderLineItem;
use App\Http\Models\Product;
use App\Http\Models\Supplier;
use App\Http\Models\Variant;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\OrderLineItemRequest;
use App\Libraries\FileHandler;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    protected $status;

    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage orders'
        ]);

        $this->status = [
            'COMPLETED',
            'CANCELED',
        ];
    }

    public function index(Request $request)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = Order::whereNull('parent_id');

        $columns = [
            'order_number', 'total',
        ];

        $tablehandler = new TableHandler();
        $items = $tablehandler->search($items, $columns, $search);
        $items = $tablehandler->sort($items, $sort);
        $items = $tablehandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        return view('pages.order.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
    }

    public function create()
    {
        $customers = Customer::get();

        return view('pages.order.create', compact('customers'));
    }

    public function store(OrderRequest $request)
    {
        $data = new Order();
        $data->customer_id = $request->customer;
        $data->order_number = strtoupper($request->order_number);
        $data->order_date = $request->order_date;
        $data->status = "ACCEPTED";
        $data->address = $request->address;
        $data->created_by = Auth::user()->id;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect('order/' . $data->id)
            ->with('success', 'Order ' . strtoupper($request->order_number) . ' berhasil dibuat.');
    }

    public function show(Request $request, $id)
    {
        $order = Order::where('id', $id)->first();
        $products = Product::get();
        $variants = Variant::get();

        return view('pages.order.detail', compact('order', 'products', 'variants'));
    }

    public function edit($id)
    {
        $order = Order::where('id', $id)->first();
        $customers = Customer::get();
        $status = $this->status;

        return view('pages.order.edit', compact('customers', 'order', 'status'));
    }

    public function update(OrderRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            $order = Order::where('id', $id)->first();
            $vat = $order->vat;
            $tax = $order->tax;
            $total = $order->total;

            $discount = $order->discount;
            if (!empty($request->discount)) {
                $discount = $request->discount;
            }

            if (!empty($request->tax)) {
                switch ($request->tax_type) {
                    case 'value':
                        $vat = 0;
                        $tax = $request->tax;
                        $total = $order->subtotal - $discount + $tax;
                        break;
                    case 'percentage':
                        $vat = $request->tax;
                        $tax = $order->subtotal * $vat / 100;
                        $total = $order->subtotal - $discount + $tax;
                        break;
                }
            }

            $data = [
                'customer_id' => $request->customer,
                'order_number' => strtoupper($request->order_number),
                'order_date' => $request->order_date,
                'discount' => $discount,
                'vat' => $vat,
                'tax' => $tax,
                'total' => $total,
                'address' => $request->address,
                'status' => $this->updateStatus($order->status, $request->status),
                'updated_by' => Auth::user()->id,
            ];
            Order::where('id', $id)->update($data);
            $this->updatePriceOrder($id);

            DB::commit();

            if ($request->status != $order->status && $data['status'] == $order->status) {
                $failed = 'Status order tidak dapat diubah.';
            } else {
                $failed = null;
            }

            return redirect('order/' . $id)
                ->with('success', 'Order ' . strtoupper($request->order_number) . ' berhasil diubah.')
                ->with('failed', $failed);
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('order/' . $id)
                ->with('failed', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $orderLineItem = OrderLineItem::where('order_id', $id)->first();
        $orderOut = Order::where('parent_id', $id)->first();

        if (empty($orderLineItem) && empty($orderOut)) {
            try {
                DB::beginTransaction();

                Order::where('id', $id)->delete();
                DB::table('orders')->where('id', $id)
                    ->update(['deleted_by' => Auth::user()->id]);

                DB::commit();

                return redirect('order')
                    ->with('success', 'Order berhasil dihapus.');
            } catch (\Exception $e) {
                DB::rollback();

                return redirect('order')
                    ->with('failed', $e->getMessage());
            }
        }
        return redirect('order')
            ->with('failed', 'Order tidak dapat dihapus karena masih digunakan oleh data lain.');
    }

    public function storeOrderLineItem(OrderLineItemRequest $request, $orderID)
    {
        try {
            DB::beginTransaction();

            // If product is not existed, create the product
            $product = Product::where('name', $request->product)->first();
            if (empty($product)) {
                $product = new Product();
                $product->name = strtoupper($request->product);
                $product->created_by = Auth::user()->id;
                $product->updated_by = Auth::user()->id;
                $product->save();
            }

            // If variant is not existed, create the variant
            if (!empty($request->variant)) {
                $variant = Variant::where('name', $request->variant)->first();
                if (empty($variant)) {
                    $variant = new Variant();
                    $variant->product_id = $variant->product_id;
                    $variant->name = strtoupper($request->variant);
                    $variant->created_by = Auth::user()->id;
                    $variant->updated_by = Auth::user()->id;
                    $variant->save();
                }
            }

            // Create the line item
            $orderLineItem = OrderLineItem::where('order_id', $orderID)
                ->where('product_id', $product->id);
            if (!empty($request->variant)) {
                $orderLineItem = $orderLineItem->where('variant_id', $variant->id);
            } else {
                $orderLineItem = $orderLineItem->whereNull('variant_id');
            }
            $orderLineItem = $orderLineItem->first();
            if (empty($orderLineItem)) {
                $orderLineItem = new OrderLineItem();
                $orderLineItem->order_id = $orderID;
                $orderLineItem->product_id = $product->id;
                if (!empty($request->variant)) {
                    $orderLineItem->variant_id = $variant->id;
                }
                $orderLineItem->qty = $request->qty;
                $orderLineItem->unit = $request->unit;
                $orderLineItem->price = $request->price;
                $orderLineItem->created_by = Auth::user()->id;
                $orderLineItem->updated_by = Auth::user()->id;
                $orderLineItem->save();
            } else {
                $data = [
                    'qty' => $request->qty + $orderLineItem->qty,
                    'price' => $request->price,
                    'status' => 'PROCESSING',
                    'updated_by' => Auth::user()->id,
                ];
                $orderLineItem->update($data);
            }
            $this->updatePriceOrder($orderID);

            DB::commit();

            return redirect('order/' . $orderID)
                ->with('success', 'Line item ' . ucwords(strtolower($request->product)) . ' berhasil dibuat.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('order/' . $orderID)
                ->with('failed', $e->getMessage());
        }
    }

    public function destroyOrderLineItem($orderID, $orderLineItemID)
    {
        try {
            DB::beginTransaction();

            OrderLineItem::where('id', $orderLineItemID)->delete();
            DB::table('order_line_items')->where('id', $orderLineItemID)
                ->update(['deleted_by' => Auth::user()->id]);
            $this->updatePriceOrder($orderID);

            DB::commit();

            return redirect('order/' . $orderID)
                ->with('success', 'Detil Order berhasil dihapus.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('order/' . $orderID)
                ->with('failed', $e->getMessage());
        }
    }

    public function createOrderOut($id)
    {
        $order = Order::where('id', $id)->first();
        if (!empty($order->child)) {
            return redirect('order/' . $id . '/out');
        }
        $suppliers = Supplier::get();

        return view('pages.order.out.create', compact('order', 'suppliers'));
    }

    public function storeOrderOut(OrderRequest $request, $id)
    {
        $data = new Order();
        $data->parent_id = $id;
        $data->supplier_id = $request->supplier;
        $data->order_number = strtoupper($request->order_number);
        $data->order_date = $request->order_date;
        $data->address = $request->address;
        $data->status = "IN PROGRESS";
        $data->created_by = Auth::user()->id;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect('order/' . $id . '/out')
            ->with('success', 'PO Keluar ' . strtoupper($request->order_number) . ' berhasil dibuat.');
    }

    public function getOrderOut(Request $request, $id)
    {
        $order = Order::where('id', $id)->first();
        if (empty($order->child)) {
            return redirect('order/' . $id . '/out/create');
        }

        $products = Product::get();
        $variants = Variant::get();

        return view('pages.order.out.index', compact('order', 'products', 'variants'));
    }

    public function storeOrderOutLineItem(OrderLineItemRequest $request, $orderID, $orderOutID)
    {
        try {
            DB::beginTransaction();

            // If product is not existed, create the product
            $product = Product::where('name', $request->product)->first();
            if (empty($product)) {
                $product = new Product();
                $product->name = strtoupper($request->product);
                $product->created_by = Auth::user()->id;
                $product->updated_by = Auth::user()->id;
                $product->save();
            }

            // If variant is not existed, create the variant
            if (!empty($request->variant)) {
                $variant = Variant::where('name', $request->variant)->first();
                if (empty($variant)) {
                    $variant = new Variant();
                    $variant->product_id = $product->id;
                    $variant->name = strtoupper($request->variant);
                    $variant->created_by = Auth::user()->id;
                    $variant->updated_by = Auth::user()->id;
                    $variant->save();
                }
            }

            // If image is existing, handle the image
            if ($request->hasfile('image')) {
                $fileHandler = new FileHandler();
                $imageName = time() . '_' . $request->file('image')->getClientOriginalName();
                $imagePath = 'uploads/order/' . $orderID . '/out/' . $orderOutID . '/' . $imageName;
                $resp = $fileHandler->save($request->file('image'), $imagePath, 'image');
                if (!$resp) {
                    throw new \Exception();
                }
            }

            // Create the line item
            $orderLineItem = OrderLineItem::where('order_id', $orderOutID)
                ->where('product_id', $product->id);
            if (!empty($request->variant)) {
                $orderLineItem = $orderLineItem->where('variant_id', $variant->id);
            } else {
                $orderLineItem = $orderLineItem->whereNull('variant_id');
            }
            $orderLineItem = $orderLineItem->first();
            if (empty($orderLineItem)) {
                $orderLineItem = new OrderLineItem();
                $orderLineItem->order_id = $orderOutID;
                $orderLineItem->product_id = $product->id;
                if (!empty($request->variant)) {
                    $orderLineItem->variant_id = $variant->id;
                }
                $orderLineItem->qty = $request->qty;
                $orderLineItem->unit = $request->unit;
                $orderLineItem->price = $request->price;
                $orderLineItem->description = $request->description;
                if ($request->hasFile('image')) {
                    $orderLineItem->image = $imageName;
                }
                $orderLineItem->created_by = Auth::user()->id;
                $orderLineItem->updated_by = Auth::user()->id;
                $orderLineItem->save();
            } else {
                $data = [
                    'qty' => $request->qty + $orderLineItem->qty,
                    'price' => $request->price,
                    'description' => $request->description,
                    'updated_by' => Auth::user()->id,
                ];

                if ($request->hasFile('image')) {
                    $data['image'] = $imageName;
                }
                $orderLineItem->update($data);
            }
            $this->updatePriceOrder($orderOutID);

            DB::commit();

            return redirect('order/' . $orderID . '/out')
                ->with('success', 'Line item ' . ucwords(strtolower($request->product)) . ' berhasil dibuat.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('order/' . $orderID . '/out')
                ->with('failed', $e->getMessage());
        }
    }

    public function editOrderOut($id)
    {
        $order = Order::where('id', $id)->first();
        $suppliers = Supplier::get();
        $status = $this->status;

        return view('pages.order.out.edit', compact('order', 'suppliers', 'status'));
    }

    public function updateOrderOut(OrderRequest $request, $orderID, $orderOutID)
    {
        try {
            DB::beginTransaction();

            $order = Order::where('id', $orderOutID)->first();
            $vat = $order->vat;
            $tax = $order->tax;
            $total = $order->total;

            $discount = $order->discount;
            if (!empty($request->discount)) {
                $discount = $request->discount;
            }

            if (!empty($request->tax)) {
                switch ($request->tax_type) {
                    case 'value':
                        $vat = 0;
                        $tax = $request->tax;
                        $total = $order->subtotal - $discount + $tax;
                        break;
                    case 'percentage':
                        $vat = $request->tax;
                        $tax = $order->subtotal * $vat / 100;
                        $total = $order->subtotal - $discount + $tax;
                        break;
                }
            }

            $data = [
                'supplier_id' => $request->supplier,
                'order_number' => strtoupper($request->order_number),
                'order_date' => $request->order_date,
                'discount' => $discount,
                'vat' => $vat,
                'tax' => $tax,
                'total' => $total,
                'address' => $request->address,
                'shipping_date' => $request->shipping_date,
                'status' => $this->updateStatus($order->status, $request->status),
                'updated_by' => Auth::user()->id,
            ];
            Order::where('id', $orderOutID)->update($data);
            $this->updatePriceOrder($orderOutID);

            DB::commit();

            if ($request->status != $order->status && $data['status'] == $order->status) {
                $failed = 'Status order tidak dapat diubah.';
            } else {
                $failed = null;
            }

            return redirect('order/' . $orderID . '/out')
                ->with('success', 'PO Keluar ' . strtoupper($request->order_number) . ' berhasil diubah.')
                ->with('failed', $failed);
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('order/' . $orderID . '/out')
                ->with('failed', $e->getMessage());
        }
    }

    public function destroyOrderOutLineItem($orderID, $orderOutID, $orderLineItemID)
    {
        try {
            DB::beginTransaction();

            OrderLineItem::where('id', $orderLineItemID)->delete();
            DB::table('order_line_items')->where('id', $orderLineItemID)
                ->update(['deleted_by' => Auth::user()->id]);
            $this->updatePriceOrder($orderOutID);

            DB::commit();

            return redirect('order/' . $orderID . '/out')
                ->with('success', 'Line item berhasil dihapus.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('order/' . $orderID . '/out')
                ->with('failed', $e->getMessage());
        }
    }

    private function updateStatus($current, $updated)
    {
        switch ($updated) {
            case 'COMPLETED':
                if ($current == 'FULFILLED' || $current == 'IN PROGRESS') {
                    return $updated;
                }
                break;
            case 'CANCELLED':
                if ($current != 'COMPLETED') {
                    return $updated;
                }
                break;
            default:
                return $current;
        }
        return $current;
    }

    private function updatePriceOrder($id)
    {
        $order = Order::where('id', $id)->first();
        $orderLineItems = OrderLineItem::where('order_id', $id)->get();
        $subtotal = 0;
        foreach ($orderLineItems as $item) {
            $subtotal += $item->price;
        }

        if (empty($order->vat)) {
            $tax = $order->tax;
        } else {
            $tax = $subtotal * ($order->vat / 100);
        }

        $total = $subtotal + $tax - $order->discount;

        $data = [
            'subtotal' => $subtotal,
            'tax' => $tax,
            'total' => $total,
            'updated_by' => Auth::User()->id,
        ];

        $order->update($data);
    }
}
