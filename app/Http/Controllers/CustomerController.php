<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Requests\CustomerRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage customers'
        ]);
    }

    public function index(Request $request)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = new Customer();

        // Setup columns in table (manual)
        $columns = [
            'name', 'email', 'address', 'phone', 'fax'
        ];

        $tablehandler = new TableHandler();
        $items = $tablehandler->search($items, $columns, $search);
        $items = $tablehandler->sort($items, $sort);
        $items = $tablehandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        return view('pages.customer.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
    }

    public function create()
    {
        return view('pages.customer.create');
    }

    public function store(CustomerRequest $request)
    {
        $data = new Customer();
        $data->name = $request->name;
        $data->email = strtoupper($request->email);
        $data->address = $request->address;
        $data->phone = $request->phone;
        $data->fax = $request->fax;
        $data->created_by = Auth::user()->id;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect('customer')
            ->with('success', 'Customer ' . $request->name . ' berhasil dibuat.');
    }

    public function edit($id)
    {
        $customer = Customer::where('id', $id)->first();

        return view('pages.customer.edit', compact('customer'));
    }

    public function update(CustomerRequest $request, $id)
    {
        $data = [
            'name' => $request->name,
            'email' => strtoupper($request->email),
            'address' => $request->address,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'updated_by' => Auth::user()->id,
        ];
        Customer::where('id', $id)->update($data);

        return redirect('customer')
            ->with('success', 'Customer ' . $request->name . ' berhasil diubah.');
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Customer::where('id', $id)->delete();
            DB::table('customers')->where('id', $id)
                ->update(['deleted_by' => Auth::user()->id]);

            DB::commit();

            return redirect('customer')
                ->with('success', 'Customer berhasil dihapus.');
        } catch (\Exception $e) {
            return redirect('customer')
                ->with('failed', $e->getMessage());
        }
    }
}
