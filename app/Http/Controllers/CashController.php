<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Cash;
use App\Http\Requests\CashRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CashController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage cashes',
        ]);
    }

    public function index(Request $request)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = new Cash();

        $columns = [
            'transaction_date', 'type', 'description', 'income', 'expense'
        ];

        $tablehandler = new TableHandler();
        $items = $tablehandler->search($items, $columns, $search);
        $items = $tablehandler->sort($items, $sort);
        $items = $tablehandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        return view('pages.cash.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
    }

    public function store(CashRequest $request)
    {
        $type = $request->type;

        $data = new Cash();
        $data->transaction_date = date('Y-m-d');
        $data->description = $request->description;
        $data->type = strtoupper($type);
        $data->$type = $request->total;
        $data->created_by = Auth::user()->id;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect('cash')
            ->with('success', 'Kas ' . ucwords(strtolower($request->description)) . ' berhasil dibuat.');;
    }

    public function edit($id)
    {
        $cash = Cash::where('id', $id)->first();

        return view('pages.cash.edit', compact('cash'));
    }

    public function update(CashRequest $request, $id)
    {
        $data = [
            'description' => $request->description,
            'type' => strtoupper($request->type),
        ];
        if ($request->type == 'expense') {
            $data['income'] = 0;
            $data['expense'] = $request->total;
        } else {
            $data['income'] = $request->total;
            $data['expense'] = 0;
        }
        Cash::where('id', $id)->update($data);

        return redirect('cash')
            ->with('success', 'Kas ' . ucwords(strtolower($request->description)) . ' berhasil diubah.');;
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Cash::where('id', $id)->delete();
            DB::table('cashes')->where('id', $id)
                ->update(['deleted_by' => Auth::user()->id]);

            DB::commit();

            return redirect('cash')
                ->with('success', 'Kas berhasil dihapus.');
        } catch (\Exception $e) {
            return redirect('cash')
                ->with('failed', $e->getMessage());
        }
    }
}
