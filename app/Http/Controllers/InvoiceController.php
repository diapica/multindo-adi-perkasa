<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Order;
use App\Http\Models\Invoice;
use App\Http\Requests\InvoiceRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{
	public function __construct()
	{
		$this->middleware([
			'auth',
			'permission:manage invoices'
		]);
	}

	public function index(Request $request)
	{
		// Parameter
		$page = $request->page;
		$search = $request->search;
		$show = $request->show;
		if (empty($show)) {
			$show = 10;
		}
		$sort = $request->sort;
		if (empty($sort)) {
			$sort = 'default';
		}
		$token = $request->_token;

		// Initial query
		$items = new Invoice();

		$columns = [
			'invoice_number', 'department',
		];

		$tablehandler = new TableHandler();
		$items = $tablehandler->search($items, $columns, $search);
		$items = $tablehandler->sort($items, $sort);
		$items = $tablehandler->paginate($items, $show);

		// Iterator
		$iterator = $page * $show;
		if ($iterator >= $show) {
			$iterator = $iterator - $show;
		}

		return view('pages.invoice.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
	}

	public function create()
	{
		$invoice = Invoice::select('order_id')->get();
		$orders = Order::WhereNotIn('id', $invoice)
			->whereNull('parent_id')
			->get();

		return view('pages.invoice.create', compact('orders'));
	}

	public function store(InvoiceRequest $request)
	{
		$order = Order::where('id', $request->order)->first();

		$data = new Invoice();
		$data->customer_id = $order->customer_id;
		$data->order_id = $request->order;
		$data->invoice_number = strtoupper($request->invoice_number);
		$data->invoice_date = $request->invoice_date;
		$data->payment_terms = $request->payment_terms;
		$data->department = $request->department;
		$data->created_by = Auth::user()->id;
		$data->updated_by = Auth::user()->id;
		$data->save();

		return redirect('invoice/' . $data->id)
			->with('success', 'Invoice ' . strtoupper($request->invoice_number) . ' berhasil dibuat.');
	}

	public function show(Request $request, $id)
	{
		$invoice = Invoice::where('id', $id)->first();
		$order = Order::where('id', $invoice->order_id)->first();

		return view('pages.invoice.detail', compact('invoice', 'order'));
	}

	public function edit($id)
	{
		$invoice = Invoice::where('id', $id)->first();
		$order = Order::where('id', $invoice->order_id)->first();

		return view('pages.invoice.edit', compact('invoice', 'order'));
	}

	public function update(InvoiceRequest $request, $id)
	{
		$data = [
			'invoice_number' => strtoupper($request->invoice_number),
			'invoice_date' => $request->invoice_date,
			'payment_terms' => $request->payment_terms,
			'department' => $request->department,
			'updated_by' => Auth::user()->id,
		];
		Invoice::where('id', $id)->update($data);

		return redirect('invoice/' . $id)
			->with('success', 'Invoice ' . strtoupper($request->invoice_number) . ' berhasil diubah.');
	}

	public function destroy($id)
	{
		try {
			DB::beginTransaction();

			Invoice::where('id', $id)->delete();
			DB::table('invoices')->where('id', $id)
				->update(['deleted_by' => Auth::user()->id]);

			DB::commit();

			return redirect('invoice')
				->with('success', 'Invoice berhasil dihapus.');
		} catch (\Exception $e) {
			DB::rollback();

			return redirect('invoice')
				->with('failed', $e->getMessage());
		}
	}
}
