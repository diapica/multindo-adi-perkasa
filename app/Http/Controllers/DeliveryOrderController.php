<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Customer;
use App\Http\Models\DeliveryOrder;
use App\Http\Models\DeliveryOrderLineItem;
use App\Http\Models\Order;
use App\Http\Models\OrderLineItem;
use App\Http\Models\Product;
use App\Http\Models\Variant;
use App\Http\Requests\DeliveryOrderRequest;
use App\Http\Requests\DeliveryOrderLineItemRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeliveryOrderController extends Controller
{
	public function __construct()
	{
		$this->middleware([
			'auth',
			'permission:manage deliveries'
		]);
	}

	public function index(Request $request)
	{
		// Parameter
		$page = $request->page;
		$search = $request->search;
		$show = $request->show;
		if (empty($show)) {
			$show = 10;
		}
		$sort = $request->sort;
		if (empty($sort)) {
			$sort = 'default';
		}
		$token = $request->_token;

		// Initial query
		$items = new DeliveryOrder();

		$columns = [
			'delivery_number',
		];

		$tablehandler = new TableHandler();
		$items = $tablehandler->search($items, $columns, $search);
		$items = $tablehandler->sort($items, $sort);
		$items = $tablehandler->paginate($items, $show);

		// Iterator
		$iterator = $page * $show;
		if ($iterator >= $show) {
			$iterator = $iterator - $show;
		}

		return view('pages.delivery.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
	}

	public function create()
	{
		$customers = Customer::get();

		return view('pages.delivery.create', compact('customers'));
	}

	public function store(DeliveryOrderRequest $request)
	{
		$data = new DeliveryOrder();
		$data->customer_id = $request->customer;
		$data->delivery_number = strtoupper($request->delivery_number);
		$data->delivery_date = $request->delivery_date;
		$data->address = $request->address;
		$data->created_by = Auth::user()->id;
		$data->updated_by = Auth::user()->id;
		$data->save();

		return redirect('delivery/' . $data->id)
			->with('success', 'Surat jalan ' . strtoupper($request->delivery_number) . ' berhasil dibuat.');
	}

	public function show(Request $request, $id)
	{
		$deliveryOrder = DeliveryOrder::where('id', $id)->first();
		$orders = Order::where('status', '!=', 'DONE')
			->whereNull('parent_id')
			->where('customer_id', $deliveryOrder->customer_id)
			->get();
		$products = Product::get();
		$variants = Variant::get();
		
		return view('pages.delivery.detail', compact('deliveryOrder', 'orders', 'products', 'variants'));
	}

	public function edit($id)
	{
		$deliveryOrder = DeliveryOrder::where('id', $id)->first();
		$customers = Customer::get();

		return view('pages.delivery.edit', compact('customers', 'deliveryOrder'));
	}

	public function update(DeliveryOrderRequest $request, $id)
	{
		$data = [
			'customer_id' => $request->customer,
			'delivery_number' => strtoupper($request->delivery_number),
			'delivery_date' => $request->delivery_date,
			'address' => $request->address,
			'updated_by' => Auth::user()->id,
		];
		DeliveryOrder::where('id', $id)->update($data);

		return redirect('delivery/' . $id)
			->with('success', 'Surat jalan ' . strtoupper($request->order_number) . ' berhasil diubah.');
	}

	public function destroy($id)
	{
		$deliveryOrderLineItem = DeliveryOrderLineItem::where('delivery_order_id', $id)->first();

		if (empty($deliveryOrderLineItem)) {
			try {
				DB::beginTransaction();

				DeliveryOrder::where('id', $id)->delete();
				DB::table('delivery_orders')->where('id', $id)
					->update(['deleted_by' => Auth::user()->id]);

				DB::commit();

				return redirect('delivery')
					->with('success', 'Surat jalan berhasil dihapus.');
			} catch (\Exception $e) {
				DB::rollback();

				return redirect('delivery')
					->with('failed', $e->getMessage());
			}
		}
		return redirect('delivery')
			->with('failed', 'Surat jalan tidak dapat dihapus karena masih digunakan oleh data lain.');
	}

	public function storeDeliveryLineItem(DeliveryOrderLineItemRequest $request, $deliveryOrderID)
	{
		$orderLineItem = OrderLineItem::where('order_id', $request->order)
			->where('product_id', $request->product);
		if (!empty($request->variant)) {
			$orderLineItem = $orderLineItem->where('variant_id', $request->variant);
		} else {
			$orderLineItem = $orderLineItem->whereNull('variant_id');
		}
		$orderLineItem = $orderLineItem->first();
		if (!empty($orderLineItem)) {
			try {
				DB::beginTransaction();

				$deliveryLineItem = DeliveryOrderLineItem::where('order_id', $request->order)
					->where('order_line_item_id', $orderLineItem->id)
					->first();
				if (empty($deliveryLineItem)) {
					$deliveryLineItem = new DeliveryOrderLineItem();
					$deliveryLineItem->delivery_order_id = $deliveryOrderID;
					$deliveryLineItem->order_id = $request->order;
					$deliveryLineItem->order_line_item_id = $orderLineItem->id;
					$deliveryLineItem->qty = $request->qty;
					$deliveryLineItem->created_by = Auth::user()->id;
					$deliveryLineItem->updated_by = Auth::user()->id;
					$deliveryLineItem->save();
				} else {
					$data = [
						'qty' => $request->qty + $deliveryLineItem->qty,
						'updated_by' => Auth::user()->id,
					];
					$deliveryLineItem->update($data);
				}

				// Check wether the order's line item is fulfilled or not
				$status = 'FULFILLED';
				$deliveryOrderLineItems = DeliveryOrderLineItem::where('order_id', $request->order)->get();
				$orderLineItems = OrderLineItem::where('order_id', $request->order)->get();
				foreach ($orderLineItems as $orderLineItem) {
					foreach ($deliveryOrderLineItems as $deliveryOrderLineItem) {
						if ($orderLineItem->id == $deliveryOrderLineItem->order_line_item_id && $orderLineItem->qty > $deliveryOrderLineItem->qty) {
							$status = 'PROCESSING';
							break;
						}
					}
				}

				// Update order status
				Order::where('id', $request->order)->update([
					'status' => $status,
					'updated_by' => Auth::user()->id,
				]);

				DB::commit();

				return redirect('delivery/' . $deliveryOrderID)
					->with('success', 'Line item berhasil dibuat.');
			} catch (\Exception $e) {
				DB::rollback();

				return redirect('delivery/' . $deliveryOrderID)
					->with('failed', $e->getMessage());
			}
		}
		return redirect('delivery/' . $deliveryOrderID)
			->with('failed', 'Produk / varian yang dimasukkan tidak sesuai dengan nomor order yang dipilih.');
	}

	public function destroyDeliveryLineItem($deliveryOrderID, $deliveryOrderLineItemID)
	{
		try {
			DB::beginTransaction();

			// Set temporary order ID for order status update
			$deliveryOrderLineItem = DeliveryOrderLineItem::where('id', $deliveryOrderLineItemID)->first();
			$orderID = $deliveryOrderLineItem->order_id;

			DeliveryOrderLineItem::where('id', $deliveryOrderLineItemID)->delete();
			DB::table('delivery_order_line_items')->where('id', $deliveryOrderLineItemID)
				->update(['deleted_by' => Auth::user()->id]);

			// Check wether the order's line item is fulfilled or not (should be still fulfilled if the qty is over, even when the item deleted)
			$status = 'FULFILLED';
			$deliveryOrderLineItems = DeliveryOrderLineItem::where('order_id', $orderID)->get();
			if (count($deliveryOrderLineItems) == 0) {
				$status = 'PROCESSING';
			} else {
				$orderLineItems = OrderLineItem::where('order_id', $orderID)->get();
				foreach ($orderLineItems as $orderLineItem) {
					foreach ($deliveryOrderLineItems as $deliveryOrderLineItem) {
						if ($orderLineItem->id == $deliveryOrderLineItem->order_line_item_id && $orderLineItem->qty > $deliveryOrderLineItem->qty) {
							$status = 'PROCESSING';
							break;
						}
					}
				}
			}

			// Update order status
			Order::where('id', $orderID)->update([
				'status' => $status,
				'updated_by' => Auth::user()->id,
			]);

			DB::commit();

			return redirect('delivery/' . $deliveryOrderID)
				->with('success', 'Line item berhasil dihapus.');
		} catch (\Exception $e) {
			DB::rollback();

			return redirect('delivery/' . $deliveryOrderID)
				->with('failed', $e->getMessage());
		}
	}
}
