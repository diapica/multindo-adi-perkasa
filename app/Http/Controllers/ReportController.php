<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Cash;
use App\Http\Models\DeliveryOrder;
use App\Http\Models\Order;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage reports',
        ]);
    }

    public function index()
    {
        return view('pages.report.index');
    }

    public function getReport(Request $request, $type)
    {
        $since = date('Y-m-01');
        if (!empty($request->since)) {
            $since = $request->since;
        }
        $until = date('Y-m-t');
        if (!empty($request->until)) {
            $until = $request->until;
        }

        switch ($type) {
            case 'order-in':
                $breadcrumbs = 'report-order-in';
                $page = 'order';
                $items = Order::whereNull('parent_id')
                    ->whereBetween('order_date', [$since, $until]);
                break;
            case 'order-out':
                $breadcrumbs = 'report-order-out';
                $page = 'order';
                $items = Order::whereNotNull('parent_id')
                    ->whereBetween('order_date', [$since, $until]);
                break;
            case 'delivery':
                $breadcrumbs = 'report-delivery';
                $page = 'delivery';
                $items = DeliveryOrder::whereBetween('delivery_date', [$since, $until]);
                break;
            case 'cash':
                $breadcrumbs = 'report-cash';
                $page = 'cash';
                $items = Cash::whereBetween('transaction_date', [$since, $until]);
                break;
            case 'profit':
                $breadcrumbs = 'report-profit';
                $page = 'profit';
                $items = Order::whereNull('parent_id')
                    ->whereBetween('order_date', [$since, $until]);
                break;
        }

        $items = $items->get();

        return view('pages.report.' . $page, compact('breadcrumbs', 'items', 'since', 'until', 'type'));
    }
}
