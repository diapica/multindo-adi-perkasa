<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use App\Http\Requests\RoleRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage roles',
        ]);
    }

    public function index(Request $request)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = Role::where('id', '!=', 1);

        // Setup columns in table (manual)
        $columns = [
            'name',
        ];

        $tableHandler = new TableHandler();
        $items = $tableHandler->search($items, $columns, $search);
        $items = $tableHandler->sort($items, $sort);
        $items = $tableHandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        return view('pages.user.role.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
    }

    public function store(RoleRequest $request)
    {
        $data = new Role();
        $data->name = strtolower($request->name);
        $data->guard_name = 'web';
        $data->save();

        return redirect('user/role/' . $data->id . '/edit')
            ->with('success', 'Role ' . ucwords(strtolower($request->name)) . ' berhasil dibuat.');
    }

    public function edit($id)
    {
        $role = Role::where('id', $id)->first();
        $permissions = Permission::where('name', '!=', 'manage permissions')->get();

        return view('pages.user.role.edit', compact('permissions', 'role'));
    }

    public function update(RoleRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            $data['name'] = strtolower($request->name);
            Role::where('id', $id)->update($data);

            $role = Role::where('id', $id)->first();
            $role->syncPermissions($request->permission);

            DB::commit();

            return redirect('user/role')
                ->with('success', 'Role ' . ucwords(strtolower($request->name)) . ' berhasil diubah.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('user/role')
                ->with('failed', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $exist = false;
        $users = User::get();
        foreach ($users as $user) {
            foreach ($user->getRoleNames() as $role) {
                $role = Role::where('name', $role)->first();
                if ($role->id == $id) {
                    $exist = true;
                    break;
                }
            }
        }

        if (!$exist) {
            Role::where('id', $id)->delete();
            return redirect('user/role')
                ->with('success', 'Role berhasil dihapus.');
        }

        return redirect('user/role')
            ->with('failed', 'Role tidak dapat dihapus karena masih digunakan oleh data lain.');
    }
}
