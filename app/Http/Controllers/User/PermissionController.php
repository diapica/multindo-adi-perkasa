<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
	public function __construct()
	{
		$this->middleware([
			'auth',
			'permission:manage privileges',
		]);
	}

	public function index(Request $request)
	{
		// Parameter
		$page = $request->page;
		$search = $request->search;
		$show = $request->show;
		if (empty($show)) {
			$show = 10;
		}
		$sort = $request->sort;
		if (empty($sort)) {
			$sort = 'default';
		}
		$token = $request->_token;

		// Initial query
		$items = new Permission();

		// Setup columns in table (manual)
		$columns = [
			'name',
		];

		$tableHandler = new TableHandler();
		$items = $tableHandler->search($items, $columns, $search);
		$items = $tableHandler->sort($items, $sort);
		$items = $tableHandler->paginate($items, $show);

		// Iterator
		$iterator = $page * $show;
		if ($iterator >= $show) {
			$iterator = $iterator - $show;
		}

		return view('pages.user.permission.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
	}

	public function store(PermissionRequest $request)
	{
		$data = new Permission();
		$data->name = strtoupper($request->name);
		$data->guard_name = 'web';
		$data->save();

		return redirect('user/permission')
			->with('success', 'Permission ' . ucwords(strtolower($request->name)) . ' berhasil dibuat.');
	}

	public function edit($id)
	{
		$permission = Permission::where('id', $id)->first();

		return view('pages.user.permission.edit', compact('permission'));
	}

	public function update(PermissionRequest $request, $id)
	{
		$data['name'] = strtolower($request->name);
		Permission::where('id', $id)->update($data);

		return redirect('user/permission')
			->with('success', 'Permission ' . ucwords(strtolower($request->name)) . ' berhasil diubah.');
	}

	public function destroy($id)
	{
		$rolePermissions = DB::table('role_has_permissions')->where('permission_id', $id)->get();
		if (count($rolePermissions) == 0) {
			Permission::where('id', $id)->delete();

			return redirect('user/permission')
				->with('success', 'Permission berhasil dihapus.');
		}

		return redirect('user/permission')
			->with('failed', 'Permission tidak dapat dihapus karena masih digunakan oleh data lain.');
	}
}
