<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use App\Http\Requests\UserRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage members',
        ]);
    }

    public function index(Request $request)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = User::where('id', '!=', 1);

        // Setup columns in table (manual)
        $columns = [
            'username', 'name',
        ];

        $tableHandler = new TableHandler();
        $items = $tableHandler->search($items, $columns, $search);
        $items = $tableHandler->sort($items, $sort);
        $items = $tableHandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        $roles = Role::where('id', '!=', 1)
            ->where('id', '!=', 2)
            ->get();

        return view('pages.user.member.index', compact('items', 'iterator', 'page', 'roles', 'search', 'show', 'sort', 'token'));
    }

    public function store(UserRequest $request)
    {
        try {
            DB::beginTransaction();

            $data = new User();
            $data->name = strtoupper($request->name);
            $data->password = Hash::make('test1234');
            $data->username = $request->username;
            $data->save();

            $role = Role::where('id', $request->role)->first();
            $data->assignRole($role->name);

            DB::commit();

            return redirect('user/member')
                ->with('success', 'Member ' . ucwords(strtolower($request->name)) . ' berhasil dibuat.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('user/member')
                ->with('failed', $e->getMessage());
        }
    }

    public function edit($id)
    {
        $member = User::where('id', $id)->first();
        foreach ($member->getRoleNames() as $role) {
            $member->role_id = Role::where('name', $role)->first()->id;
        }
        $roles = Role::where('id', '!=', 1)
            ->where('id', '!=', 2)
            ->get();

        return view('pages.user.member.edit', compact('member', 'roles'));
    }

    public function update(UserRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            $data['name'] = strtoupper($request->name);
            $data['username'] = $request->username;
            User::where('id', $id)->update($data);

            $role = Role::where('id', $request->role)->first();
            $user = User::where('id', $id)->first();
            $user->syncRoles([$role->name]);

            DB::commit();

            return redirect('user/member')
                ->with('success', 'Member ' . ucwords(strtolower($request->name)) . ' berhasil diubah.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('user/member')
                ->with('failed', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        /**
         * TODO:
         * Check wether the user is still being used in any table or not
         */

        User::where('id', $id)->delete();

        return redirect('user/member')
            ->with('success', 'Member berhasil dihapus.');
    }
}
