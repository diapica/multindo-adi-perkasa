<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use App\Http\Requests\MemberRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Initial query
        $item = User::where('id', Auth::user()->id)->first();

        return view('pages.user.profile.index', compact('item'));
    }

    public function edit($id)
    {
        $profile = User::where('id', Auth::user()->id)->first();

        return view('pages.user.profile.edit', compact('profile'));
    }

    public function update(MemberRequest $request, $id)
    {
        $data['name'] = strtoupper($request->name);
        $data['username'] = $request->username;
        User::where('id', $id)->update($data);

        return redirect('user/profile')
            ->with('success', 'Profile ' . ucwords(strtolower($request->name)) . ' updated successfully');
    }

    public function password()
    {
        $profile = User::where('id', Auth::user()->id)->first();

        return view('pages.user.profile.password', compact('profile'));
    }

    public function updatePassword(PasswordRequest $request, $id)
    {
        if (Hash::check($request->old_password, Auth::user()->password)) {
            $data['password'] = Hash::make($request->password);
            User::where('id', $id)->update($data);

            return redirect('user/profile')
                ->with('success', 'Password berhasil diubah.');
        }
        return redirect('user/profile/' . $id . '/password')
            ->with('failed', 'Password tidak cocok.');
    }
}
