<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Imports\BaseImport;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages.index');
    }

    public function upload(Request $request)
    {
        try {
            $import = new BaseImport();
            $import->import($request->file('upload'));

            return redirect('/')
                ->with('success', 'Data berhasil diunggah.');
        } catch (\Exception $e) {
            return redirect('/')
                ->with('failed', $e->getMessage());
        }
    }
}
