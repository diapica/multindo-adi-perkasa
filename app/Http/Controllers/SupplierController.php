<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Supplier;
use App\Http\Requests\SupplierRequest;
use App\Libraries\TableHandler;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth',
            'permission:manage suppliers'
        ]);
    }

    public function index(Request $request)
    {
        // Parameter
        $page = $request->page;
        $search = $request->search;
        $show = $request->show;
        if (empty($show)) {
            $show = 10;
        }
        $sort = $request->sort;
        if (empty($sort)) {
            $sort = 'default';
        }
        $token = $request->_token;

        // Initial query
        $items = new Supplier();

        // Setup columns in table (manual)
        $columns = [
            'name', 'email', 'address', 'phone', 'fax',
        ];

        $tablehandler = new TableHandler();
        $items = $tablehandler->search($items, $columns, $search);
        $items = $tablehandler->sort($items, $sort);
        $items = $tablehandler->paginate($items, $show);

        // Iterator
        $iterator = $page * $show;
        if ($iterator >= $show) {
            $iterator = $iterator - $show;
        }

        return view('pages.supplier.index', compact('items', 'iterator', 'page', 'search', 'show', 'sort', 'token'));
    }

    public function create()
    {
        return view('pages.supplier.create');
    }

    public function store(SupplierRequest $request)
    {
        $data = new Supplier();
        $data->name = $request->name;
        $data->email = strtoupper($request->email);
        $data->address = $request->address;
        $data->phone = $request->phone;
        $data->fax = $request->fax;
        $data->created_by = Auth::user()->id;
        $data->updated_by = Auth::user()->id;
        $data->save();

        return redirect('supplier')
            ->with('success', 'Supplier ' . $request->name . ' berhasil dibuat.');
    }

    public function edit($id)
    {
        $supplier = Supplier::where('id', $id)->first();

        return view('pages.supplier.edit', compact('supplier'));
    }

    public function update(SupplierRequest $request, $id)
    {
        $data = [
            'name' => $request->name,
            'email' => strtoupper($request->email),
            'address' => $request->address,
            'phone' => $request->phone,
            'fax' => $request->fax,
            'updated_by' => Auth::user()->id,
        ];
        Supplier::where('id', $id)->update($data);

        return redirect('supplier')
            ->with('success', 'Supplier ' . $request->name . ' berhasil diubah.');
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            Supplier::where('id', $id)->delete();
            DB::table('suppliers')->where('id', $id)
                ->update(['deleted_by' => Auth::user()->id]);

            DB::commit();

            return redirect('supplier')
                ->with('success', 'Supplier berhasil dihapus.');
        } catch (\Exception $e) {
            DB::rollback();

            return redirect('supplier')
                ->with('failed', $e->getMessage());
        }
    }
}
