<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'customer_id',
		'order_id',
		'invoice_number',
		'invoice_date',
		'payment_terms',
		'department',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function customer()
	{
		return $this->belongsTo('App\Http\Models\Customer', 'customer_id');
	}
	public function order()
	{
		return $this->belongsTo('App\Http\Models\Order', 'order_id');
	}
	public function createdBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'created_by');
	}
	public function updatedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'updated_by');
	}
	public function deletedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'deleted_by');
	}
}
