<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryOrderLineItem extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'delivery_order_id',
		'order_id',
		'order_line_item_id',
		'qty',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function order()
	{
		return $this->belongsTo('App\Http\Models\Order', 'order_id');
	}
	public function orderLineItem()
	{
		return $this->belongsTo('App\Http\Models\OrderLineItem', 'order_line_item_id');
	}
	public function createdBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'created_by');
	}
	public function updatedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'updated_by');
	}
	public function deletedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'deleted_by');
	}
}
