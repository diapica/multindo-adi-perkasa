<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryOrder extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'customer_id',
		'delivery_number',
		'delivery_date',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function customer()
	{
		return $this->belongsTo('App\Http\Models\Customer', 'customer_id');
	}
	public function lineItems()
	{
		return $this->hasMany('App\Http\Models\DeliveryOrderLineItem');
	}
	public function createdBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'created_by');
	}
	public function updatedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'updated_by');
	}
	public function deletedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'deleted_by');
	}
}
