<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'parent_id',
        'customer_id',
        'supplier_id',
        'order_number',
        'order_date',
        'subtotal',
        'discount',
        'vat',
        'tax',
        'total',
        'shipping_date',
        'address',
        'payment_terms',
        'status',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function parent()
    {
        return $this->belongsTo('App\Http\Models\Order', 'parent_id');
    }
    public function child()
    {
        return $this->hasOne('App\Http\Models\Order', 'parent_id');
    }
    public function customer()
    {
        return $this->belongsTo('App\Http\Models\Customer', 'customer_id');
    }
    public function supplier()
    {
        return $this->belongsTo('App\Http\Models\Supplier', 'supplier_id');
    }
    public function lineItems()
    {
        return $this->hasMany('App\Http\Models\OrderLineItem');
    }
    public function invoice()
    {
        return $this->hasOne('App\Http\Models\Invoice');
    }
    public function createdBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }
    public function updatedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }
    public function deletedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
