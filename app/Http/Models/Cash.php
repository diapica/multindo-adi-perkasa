<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Cash extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'transaction_date',
		'type',
		'description',
		'income',
		'expense',
		'created_by',
		'updated_by',
		'deleted_by',
	];

	public function createdBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'created_by');
	}
	public function updatedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'updated_by');
	}
	public function deletedBy()
	{
		return $this->belongsTo('App\Http\Models\User', 'deleted_by');
	}
}
