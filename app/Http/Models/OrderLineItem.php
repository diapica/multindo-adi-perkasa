<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderLineItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'order_id',
        'product_id',
        'variant_id',
        'description',
        'qty',
        'unit',
        'price',
        'image',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function order()
    {
        return $this->belongsTo('App\Http\Models\Order', 'order_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Http\Models\Product', 'product_id');
    }
    public function variant()
    {
        return $this->belongsTo('App\Http\Models\Variant', 'variant_id');
    }
    public function createdBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }
    public function updatedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }
    public function deletedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
