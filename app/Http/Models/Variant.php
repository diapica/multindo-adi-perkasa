<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variant extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id',
        'name',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    public function product()
    {
        return $this->belongsTo('App\Http\Models\Product', 'product_id');
    }
    public function createdBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by');
    }
    public function updatedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'updated_by');
    }
    public function deletedBy()
    {
        return $this->belongsTo('App\Http\Models\User', 'deleted_by');
    }
}
