<?php

namespace App\Imports;

use App\Http\Models\DeliveryOrder;
use App\Http\Models\DeliveryOrderLineItem;
use App\Http\Models\Order;
use App\Http\Models\OrderLineItem;
use App\Http\Models\Product;
use App\Http\Models\Variant;
use DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DeliveryOrderLineItemImport implements ToModel, WithHeadingRow
{
  use Importable;

  public function __construct()
  {
    //
  }

  public function model(array $row)
  {
    if (!isset($row['delivery_number']) || !isset($row['order_number']) || !isset($row['product']) || !isset($row['variant']) || !isset($row['qty'])) {
      return null;
    }

    $deliveryOrder = DeliveryOrder::where('delivery_number', $row['delivery_number'])->first();
    if (empty($deliveryOrder)) {
      return null;
    }

    $order = Order::where('order_number', $row['order_number'])->first();
    if (empty($order)) {
      return null;
    }

    $product = Product::where('id', $row['product'])->first();
    if (empty($product)) {
      return null;
    }

    $orderLineItem = OrderLineItem::where('order_id', $order->child->id)
      ->where('product_id', $product->id);
    if (isset($row['variant'])) {
      $variant = Variant::where('id', $row['variant'])->first();
      if (empty($variant)) {
        return null;
      }
      $orderLineItem = $orderLineItem->where('variant_id', $variant->id);
    }
    $orderLineItem = $orderLineItem->first();
    if (empty($orderLineItem)) {
      return null;
    }

    try {
      DB::beginTransaction();

      $data = new DeliveryOrderLineItem();
      $data->delivery_order_id = $deliveryOrder->id;
      $data->order_id = $order->id;
      $data->order_line_item_id = $orderLineItem->id;
      $data->qty = $row['qty'];
      $data->created_by = Auth::user()->id;
      $data->updated_by = Auth::user()->id;
      $data->save();

      $status = 'FULFILLED';
      $deliveryOrderLineItems = DeliveryOrderLineItem::where('order_id', $order->id)->get();
      $orderLineItems = OrderLineItem::where('order_id', $order->id)->get();
      foreach ($orderLineItems as $orderLineItem) {
        foreach ($deliveryOrderLineItems as $deliveryOrderLineItem) {
          if ($orderLineItem->id == $deliveryOrderLineItem->order_line_item_id && $orderLineItem->qty > $deliveryOrderLineItem->qty) {
            $status = 'PROCESSING';
            break;
          }
        }
      }

      // Update order status
      Order::where('id', $order->id)->update([
        'status' => $status,
        'updated_by' => Auth::user()->id,
      ]);

      DB::commit();

      return $data;
    } catch (\Exception $e) {
      DB::rollback();

      return null;
    }
  }
}
