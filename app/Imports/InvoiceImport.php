<?php

namespace App\Imports;

use App\Http\Models\Customer;
use App\Http\Models\Invoice;
use App\Http\Models\Order;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class InvoiceImport implements ToModel, WithHeadingRow
{
  use Importable;

  public function __construct()
  {
    //
  }

  public function model(array $row)
  {
    if (!isset($row['customer']) || !isset($row['order_number']) || !isset($row['invoice_number']) || !isset($row['invoice_date']) || !isset($row['payment_terms']) || !isset($row['department'])) {
      return null;
    }

    $customer = Customer::where('id', $row['customer'])->first();
    if (empty($customer)) {
      return null;
    }

    $order = Order::where('order_number', $row['order_number'])->first();
    if (empty($order)) {
      return null;
    }

    $invoice = Invoice::where('invoice_number', $row['invoice_number'])->first();
    if (!empty($invoice)) {
      return null;
    }

    $data = new Invoice();
    $data->customer_id = $customer->id;
    $data->order_id = $order->id;
    $data->invoice_number = $row['invoice_number'];
    $invoiceDate = ($row['invoice_date'] - 25569) * 86400;
    $data->invoice_date = gmdate('Y-m-d', $invoiceDate);
    $paymentTerms = ($row['payment_terms'] - 25569) * 86400;
    $data->payment_terms = gmdate('Y-m-d', $paymentTerms);
    $data->department = $row['department'];
    $data->created_by = Auth::user()->id;
    $data->updated_by = Auth::user()->id;
    $data->save();

    return $data;
  }
}
