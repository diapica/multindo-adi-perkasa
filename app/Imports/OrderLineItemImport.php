<?php

namespace App\Imports;

use App\Http\Models\Order;
use App\Http\Models\OrderLineItem;
use App\Http\Models\Product;
use App\Http\Models\Variant;
use DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OrderLineItemImport implements ToModel, WithHeadingRow
{
  use Importable;

  public function __construct()
  {
    //
  }

  public function model(array $row)
  {
    if (!isset($row['order_number']) || !isset($row['product']) || !isset($row['qty']) || !isset($row['unit']) || !isset($row['price'])) {
      return null;
    }

    $order = Order::where('order_number', $row['order_number'])->first();
    if (empty($order)) {
      return null;
    }

    $product = Product::where('id', $row['product'])->first();
    if (empty($product)) {
      return null;
    }

    try {
      DB::beginTransaction();

      $data = new OrderLineItem();
      $data->order_id = $order->id;
      $data->product_id = $product->id;
      if (isset($row['variant'])) {
        $variant = Variant::where('id', $row['variant'])->first();
        if (empty($variant)) {
          return null;
        }
        $data->variant_id = $variant->id;
      }
      $data->description = $row['description'];
      $data->qty = $row['qty'];
      $data->unit = $row['unit'];
      $data->price = $row['price'];
      $data->created_by = Auth::user()->id;
      $data->updated_by = Auth::user()->id;
      $data->save();

      $this->updatePriceOrder($order->id);
      $this->updateStatusOrder($order->id);

      DB::commit();

      return $data;
    } catch (\Exception $e) {
      DB::rollback();

      return null;
    }
  }

  private function updatePriceOrder($id)
  {
    $order = Order::where('id', $id)->first();
    $orderLineItems = OrderLineItem::where('order_id', $id)->get();
    $subtotal = 0;
    foreach ($orderLineItems as $item) {
      $subtotal += $item->price;
    }

    if (empty($order->vat)) {
      $tax = $order->tax;
    } else {
      $tax = $subtotal * ($order->vat / 100);
    }

    $total = $subtotal + $tax - $order->discount;

    $data = [
      'subtotal' => $subtotal,
      'tax' => $tax,
      'total' => $total,
      'updated_by' => Auth::User()->id,
    ];

    $order->update($data);
  }

  private function updateStatusOrder($id)
  {
    $order = Order::where('id', $id)->first();
    $status = 'PROCESSING';
    if (!empty($order->parent_id)) {
      $status = 'IN PROGRESS';
    }
    $data = [
      'status' => $status,
      'updated_by' => Auth::User()->id,
    ];
    $order->update($data);
  }
}
