<?php

namespace App\Imports;

use App\Http\Models\Customer;
use App\Http\Models\Order;
use App\Http\Models\Supplier;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OrderImport implements ToModel, WithHeadingRow
{
	use Importable;

	protected $_type;

	public function __construct(string $type)
	{
		$this->_type = $type;
	}

	public function model(array $row)
	{
		switch ($this->_type) {
			case 'in':
				if (!isset($row['customer']) || !isset($row['order_number']) || !isset($row['order_date'])) {
					return null;
				}

				$customer = Customer::where('id', $row['customer'])->first();
				if (empty($customer)) {
					return null;
				}

				$order = Order::where('order_number', $row['order_number'])->first();
				if (!empty($order)) {
					return null;
				}

				$data = new Order();
				$data->customer_id = $customer->id;
				$data->order_number = $row['order_number'];
				$orderDate = ($row['order_date'] - 25569) * 86400;
				$data->order_date = gmdate('Y-m-d', $orderDate);
				$data->vat = 0;
				$data->tax = 0;
				if (!empty($row['vat'])) {
					$data->vat = $row['vat'];
				} else if (!empty($row['tax'])) {
					$data->tax = $row['tax'];
				}
				$data->address = $row['address'];
				if (empty($row['address'])) {
					$data->address = $customer->address;
				}
				$data->status = 'ACCEPTED';
				$data->created_by = Auth::user()->id;
				$data->updated_by = Auth::user()->id;
				$data->save();

				break;
			case 'out':
				if (!isset($row['supplier']) || !isset($row['order_masuk']) || !isset($row['order_number']) || !isset($row['order_date'])) {
					return null;
				}

				$supplier = Supplier::where('id', $row['supplier'])->first();
				if (empty($supplier)) {
					return null;
				}

				$order = Order::where('order_number', $row['order_number'])->first();
				if (!empty($order)) {
					return null;
				}

				$parent = Order::where('order_number', $row['order_masuk'])->first();
				if (empty($parent)) {
					return null;
				}

				$data = new Order();
				$data->parent_id = $parent->id;
				$data->supplier_id = $supplier->id;
				$data->order_number = $row['order_number'];
				$orderDate = ($row['order_date'] - 25569) * 86400;
				$data->order_date = gmdate('Y-m-d', $orderDate);
				$data->vat = 0;
				$data->tax = 0;
				if (!empty($row['vat'])) {
					$data->vat = $row['vat'];
				} else if (!empty($row['tax'])) {
					$data->tax = $row['tax'];
				}
				$data->shipping_date = $row['shipping_date'];
				$data->address = $row['address'];
				if (empty($row['address'])) {
					$data->address = $customer->address;
				}
				$data->status = 'IN PROGRESS';
				$data->created_by = Auth::user()->id;
				$data->updated_by = Auth::user()->id;
				$data->save();

				break;
		}

		return $data;
	}
}
