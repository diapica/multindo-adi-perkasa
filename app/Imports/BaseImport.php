<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class BaseImport implements WithMultipleSheets
{
	use Importable;

	public function __construct()
	{
		//
	}

	public function sheets(): array
	{
		return [
			'Order Masuk' => new OrderImport('in'),
			'Order Keluar' => new OrderImport('out'),
			'Order Line Item' => new OrderLineItemImport(),
			'Delivery Order' => new DeliveryOrderImport(),
			'Delivery Order Line Item' => new DeliveryOrderLineItemImport(),
			'Invoice' => new InvoiceImport(),
		];
	}
}
