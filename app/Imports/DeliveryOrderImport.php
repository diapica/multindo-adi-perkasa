<?php

namespace App\Imports;

use App\Http\Models\Customer;
use App\Http\Models\DeliveryOrder;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DeliveryOrderImport implements ToModel, WithHeadingRow
{
  use Importable;

  public function __construct()
  {
    //
  }

  public function model(array $row)
  {
    if (!isset($row['customer']) || !isset($row['delivery_number']) || !isset($row['delivery_date'])) {
      return null;
    }

    $customer = Customer::where('id', $row['customer'])->first();
    if (empty($customer)) {
      return null;
    }

    $deliveryOrder = DeliveryOrder::where('delivery_number', $row['delivery_number'])->first();
    if (!empty($deliveryOrder)) {
      return null;
    }

    $data = new DeliveryOrder();
    $data->customer_id = $customer->id;
    $data->delivery_number = $row['delivery_number'];
    $deliveryDate = ($row['delivery_date'] - 25569) * 86400;
    $data->delivery_date = gmdate('Y-m-d', $deliveryDate);
    $data->address = $row['address'];
    if (empty($row['address'])) {
      $data->address = $customer->address;
    }
    $data->created_by = Auth::user()->id;
    $data->updated_by = Auth::user()->id;
    $data->save();

    return $data;
  }
}
