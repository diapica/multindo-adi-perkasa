<?php

namespace App\Libraries;

use Image;
use Storage;

class FileHandler
{
    public function save($file, $path, $type)
    {
        switch ($type) {
            case 'image':
                $size = getimagesize($file);
                $width = $size[0];
                $height = $size[1];

                if ($width == $height) {
                    $imageData = Image::make($file->getRealPath())->resize(320, 320);
                } else if ($width > $height) {
                    $imageData = Image::make($file->getRealPath())->resize(720, 509);
                } else {
                    $imageData = Image::make($file->getRealPath())->resize(509, 720);
                }

                Storage::disk('public')->put($path, (string) $imageData->encode());
                return true;
            case 'document':
                Storage::disk('public')->put($path, file_get_contents($file->getRealPath()));
                return true;
            default:
                return false;
        }
    }
}
