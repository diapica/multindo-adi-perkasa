<?php

namespace App\Libraries;

class TableHandler
{
    public function search($items, $columns, $search)
    {
        return $items->where(function ($query) use ($columns, $search) {
            $query->where($columns[0], 'LIKE', '%' . $search . '%');
            if (count($columns) > 1) {
                $i = 0;
                foreach ($columns as $column) {
                    if ($i != 0) {
                        $query->orWhere($column, 'LIKE', '%' . $search . '%');
                    }
                    $i++;
                }
            }
        });
    }

    public function searchOther($items, $columns, $search, $relation)
    {
        return $items->whereHas($relation, function ($query) use ($columns, $search) {
            $query->where($columns[0], 'LIKE', '%' . $search . '%');
            if (count($columns) > 1) {
                $i = 0;
                foreach ($columns as $column) {
                    if ($i != 0) {
                        $query->orWhere($column, 'LIKE', '%' . $search . '%');
                    }
                    $i++;
                }
            }
        });
    }

    public function sort($items, $sort)
    {
        if ($sort != 'default') {
            $sorts = explode(',', $sort);
            return $items->orderBy($sorts[0], $sorts[1]);
        }
        return $items->orderBy('id', 'asc');
    }

    public function paginate($items, $show)
    {
        if (!empty($show)) {
            return $items->paginate($show);
        }
        return $items->paginate(10);
    }
}
