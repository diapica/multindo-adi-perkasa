<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'manage members']);
        Permission::create(['name' => 'manage privileges']);
        Permission::create(['name' => 'manage roles']);
        Permission::create(['name' => 'manage customers']);
        Permission::create(['name' => 'manage suppliers']);
        Permission::create(['name' => 'manage products']);
        Permission::create(['name' => 'manage orders']);
        Permission::create(['name' => 'manage deliveries']);
        Permission::create(['name' => 'manage invoices']);
        Permission::create(['name' => 'manage cashes']);
        Permission::create(['name' => 'manage reports']);

        $role = Role::create(['name' => 'super admin']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'admin']);
        $adminPermissions = [
            'manage members',
            'manage roles',
            'manage customers',
            'manage suppliers',
            'manage products',
            'manage orders',
            'manage deliveries',
            'manage invoices',
            'manage cashes',
            'manage reports',
        ];
        $role->givePermissionTo($adminPermissions);
    }
}
