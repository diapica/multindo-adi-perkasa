<?php

use App\Http\Models\DeliveryOrder;
use App\Http\Models\DeliveryOrderLineItem;
use App\Http\Models\Order;
use Illuminate\Database\Seeder;

class DeliveryOrderTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DeliveryOrder::create([
      'customer_id' => 1,
      'delivery_number' => '190210001',
      'delivery_date' => date('Y-m-d'),
      'address' => 'Alamat Customer',
      'created_by' => 1,
      'updated_by' => 1,
    ]);

    DeliveryOrderLineItem::create([
      'delivery_order_id' => 1,
      'order_id' => 1,
      'order_line_item_id' => 1,
      'qty' => 1,
      'created_by' => 1,
      'updated_by' => 1,
    ]);

    Order::where('id', 1)->update([
      'status' => 'PROCESSING',
      'updated_by' => 1,
    ]);
  }
}
