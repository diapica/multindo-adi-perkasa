<?php

use App\Http\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$user = User::create([
			'name' => 'SUPER ADMIN',
			'username' => 'superadmin',
			'password' => Hash::make('test1234'),
		]);
		$user->assignRole('super admin');

		$user = User::create([
			'name' => 'ADMIN',
			'username' => 'admin',
			'password' => Hash::make('test1234'),
		]);
		$user->assignRole('admin');
	}
}
