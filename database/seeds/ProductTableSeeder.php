<?php

use App\Http\Models\Product;
use App\Http\Models\Variant;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create(['name' => 'CHINO PANTS', 'created_by' => '1']);
        Product::create(['name' => 'SALES FORCE PANTS', 'created_by' => '1']);

        Variant::create(['name' => 'CHINO PANTS S', 'product_id' => '1', 'created_by' => '1']);
        Variant::create(['name' => 'CHINO PANTS M', 'product_id' => '1', 'created_by' => '1']);
        Variant::create(['name' => 'CHINO PANTS L', 'product_id' => '1', 'created_by' => '1']);
        Variant::create(['name' => 'CHINO PANTS XL', 'product_id' => '1', 'created_by' => '1']);

        Variant::create(['name' => 'SALES FORCE PANTS S', 'product_id' => '2', 'created_by' => '1']);
        Variant::create(['name' => 'SALES FORCE PANTS M', 'product_id' => '2', 'created_by' => '1']);
        Variant::create(['name' => 'SALES FORCE PANTS L', 'product_id' => '2', 'created_by' => '1']);
        Variant::create(['name' => 'SALES FORCE PANTS XL', 'product_id' => '2', 'created_by' => '1']);
    }
}
