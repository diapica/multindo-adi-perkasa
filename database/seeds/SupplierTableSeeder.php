<?php

use App\Http\Models\Supplier;
use Illuminate\Database\Seeder;

class SupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Supplier::create([
            'name' => 'Dunia Konveksi',
            'email' => 'duniakonveksi@gmail.com',
            'address' => 'Jl. Nuh Raya RT/RW 006/05 No.1 Kel. Sukabumi Utara Kec. Kebon Jeruk Jakarta Barat 11540',
            'phone' => '(021) 456 708',
            'fax' => '+44161999888',
            'created_by' => '1',
            'updated_by' => '1',
        ]);

        Supplier::create([
            'name' => 'Toko Konveksi',
            'email' => 'tokokonveksi@gmail.com',
            'address' => 'Jl. Nuh Raya RT/RW 006/05 No.1 Kel. Sukabumi Utara Kec. Kebon Jeruk Jakarta Barat 11540',
            'phone' => '(021) 456 708',
            'fax' => '+44161999888',
            'created_by' => '1',
            'updated_by' => '1',
        ]);
    }
}
