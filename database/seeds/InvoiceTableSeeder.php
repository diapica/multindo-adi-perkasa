<?php

use App\Http\Models\Invoice;
use Illuminate\Database\Seeder;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invoice::create([
            'customer_id' => 1,
            'order_id' => 1,
            'invoice_number' => 'INV-009/2019',
            'invoice_date' => date('Y-m-d'),
            'payment_terms' => date('Y-m-d', strtotime('+7 days')),
            'department' => 'Finance',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
