<?php

use App\Http\Models\Order;
use App\Http\Models\OrderLineItem;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
            'customer_id' => 1,
            'order_number' => '003/AD/DPO/1/19',
            'order_date' => date('Y-m-d'),
            'subtotal' => 200000,
            'discount' => 0,
            'vat' => 0,
            'tax' => 20000,
            'total' => 220000,
            'address' => 'Alamat customer',
            'status' => 'ACCEPTED',
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        OrderLineItem::create([
            'order_id' => 1,
            'product_id' => 1,
            'variant_id' => 1,
            'description' => 'Description',
            'qty' => 10,
            'unit' => 'pcs',
            'price' => 200000,
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        Order::create([
            'parent_id' => 1,
            'supplier_id' => 1,
            'order_number' => 'OUT/003/AD/DPO/1/19',
            'order_date' => date('Y-m-d'),
            'subtotal' => 100000,
            'discount' => 0,
            'vat' => 0,
            'tax' => 10000,
            'total' => 110000,
            'address' => 'Alamat supplier',
            'status' => 'IN PROGRESS',
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        OrderLineItem::create([
            'order_id' => 2,
            'product_id' => 1,
            'variant_id' => 1,
            'description' => 'Description',
            'qty' => 10,
            'unit' => 'pcs',
            'price' => 100000,
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
