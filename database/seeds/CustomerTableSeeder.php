<?php

use App\Http\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            'name' => 'John',
            'email' => 'john@example.com',
            'address' => 'Jakarta',
            'phone' => '(021) 456 708',
            'fax' => '+44161999888',
            'created_by' => 1,
            'updated_by' => 1,
        ]);

        Customer::create([
            'name' => 'Sam',
            'email' => 'sam@example.com',
            'address' => 'Bandung',
            'phone' => '(021) 708 256',
            'fax' => '+48881619499',
            'created_by' => 1,
            'updated_by' => 1,
        ]);
    }
}
