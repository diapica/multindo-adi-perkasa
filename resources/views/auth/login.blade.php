@extends('layouts.app')

@section('content')
<h1 class="text-center">Login</h1>

<br/>

<form method="post" action="{{ route('login') }}">
  {{ csrf_field() }}
  <div class="form-group row">
    <label for="username" class="col-sm-4 col-form-label text-md-right">Username</label>
    <div class="col-md-5">
      <input type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" id="username" required autofocus>
			@if ($errors->has('username'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('username') }}</strong>
				</span>
			@endif
		</div>
  </div>
  <div class="form-group row">
    <label for="password" class="col-sm-4 col-form-label text-md-right">Password</label>
    <div class="col-md-5">
      <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" id="password" required>
			@if ($errors->has('password'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
    </div>
  </div>
	<div class="form-group row">
		<div class="col-md-5 offset-md-4">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
				</label>
			</div>
		</div>
	</div>
  <div class="form-group row mb-0">
		<div class="col-md-8 offset-md-4">
			<button type="submit" class="btn btn-secondary">Login</button>
		</div>
	</div>
</form>
@endsection
