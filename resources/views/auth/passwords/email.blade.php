@extends('layouts.app')

@section('content')

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif

<h1 class="text-center">Reset Password</h1>

<br/>

<form method="post" action="{{ route('password.email') }}">
  {{ csrf_field() }}
  <div class="form-group row">
    <label for="email" class="col-sm-4 col-form-label text-md-right">Email Address</label>
    <div class="col-md-5">
      <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" id="email" autofocus>
			@if ($errors->has('email'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
			@endif
		</div>
  </div>
  <div class="form-group row mb-0">
		<div class="col-md-8 offset-md-4">
			<button type="submit" class="btn btn-secondary">Send Reset Password Link</button>
		</div>
	</div>
</form>
@endsection
