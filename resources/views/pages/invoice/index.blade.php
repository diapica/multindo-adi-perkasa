@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('invoice') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Invoice</h1>
  </div>
</div>

<br/>

<!-- Sort & Filter -->
@component('layouts.components.sortfilter', [
  'path' => 'invoice',
  'show' => $show,
  'search' => $search,
  'createPath' => 'invoice/create'
]) @endcomponent

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <form method="get" action="{{ url('invoice') }}">
        {{ csrf_field() }}
        <input type="hidden" name="show" id="show" value="{{ $show }}">
        <input type="hidden" name="search" id="search" value="{{ $search }}">
        <input type="hidden" name="sort" id="sort" value="{{ $sort }}">
        <input type="hidden" name="page" id="page" value="{{ $page }}">
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="id">
            <span><b>No.</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="invoice_number"
            onclick="setupSort('{{ ($sort == "invoice_number,asc") ? "invoice_number,desc" : "invoice_number,asc" }}')">
            <span><b>Nomor Invoice</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'invoice_number,asc') fa-sort-up @elseif ($sort == 'invoice_number,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="order_number">
            <span><b>Nomor Order</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="customer">
            <span><b>Customer</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="invoice_date"
            onclick="setupSort('{{ ($sort == "invoice_date,asc") ? "invoice_date,desc" : "invoice_date,asc" }}')">
            <span><b>Tanggal Invoice</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'invoice_date,asc') fa-sort-up @elseif ($sort == 'invoice_date,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="payment_terms"
            onclick="setupSort('{{ ($sort == "payment_terms,asc") ? "payment_terms,desc" : "payment_terms,asc" }}')">
            <span><b>Tanggal Pembayaran</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'payment_terms,asc') fa-sort-up @elseif ($sort == 'payment_terms,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="total">
            <span><b>Total</b></span>
          </button>
        </th>
        <th></th>
      </form>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="8" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @foreach ($items as $item)
          <tr>
            <td class="text-center">{{ ++$iterator }}.</td>
            <td>{{ $item->invoice_number }}</td>
            <td><a href="{{ url('order/'.$item->order->id) }}">{{ $item->order->order_number }}</a></td>
            <td>{{ $item->customer->name }}</td>
            <td>{{ date('d F Y', strtotime($item->invoice_date)) }}</td>
            <td>{{ date('d F Y', strtotime($item->payment_terms)) }}</td>
            <td class="text-right">IDR {{ number_format($item->order->total, 2, ',', '.') }}</td>
            <td class="text-center">
              <a href="{{ url('invoice/'.$item->id) }}" class="btn btn-primary" style="color: white">
                <span class="fas fa-search" title="show"></span>
              </a>
              <button class="btn btn-danger open-confirm" data-name="{{ $item->invoice_number }}" data-url="{{ url('invoice/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
</div>

<!-- Pagination -->
@component('layouts.components.pagination', [
  'path' => 'invoice',
  'items' => $items,
  'token' => $token,
  'show' => $show,
  'search' => $search,
  'sort' => $sort
]) @endcomponent

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop