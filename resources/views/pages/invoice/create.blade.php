@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('invoice-create') }}

<h1 class="text-center">Tambah Invoice</h1>

<br/>

<form method="post" action="{{ url('invoice') }}">
  {{ csrf_field() }}
  <div class="form-group row">
    <label for="order" class="col-md-4 col-form-label text-md-right">Order</label>
    <div class="col-md-6">
      <select class="form-control" name="order" id="order">
        <option disabled @if (empty(old('order'))) selected @endif>Pilih Order</option>
        @foreach ($orders as $item)
          <option value="{{ $item->id }}" @if (old('order') == $item->id) selected @endif>{{ $item->order_number }}</option>
        @endforeach
      </select>
      @if ($errors->has('order'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('order') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="invoice_number" class="col-md-4 col-form-label text-md-right">Nomor Invoice</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('invoice_number') ? 'is-invalid' : '' }}" name="invoice_number" id="invoice_number" placeholder="Ex : 001/AD/DPO/1/19" value="{{ old('invoice_number') }}">
      @if ($errors->has('invoice_number'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('invoice_number') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="invoice_date" class="col-md-4 col-form-label text-md-right">Tanggal Invoice</label>
    <div class="col-md-6">
      <input type="date" class="form-control {{ $errors->has('invoice_date') ? 'is-invalid' : '' }}" name="invoice_date" id="invoice_date" placeholder="yyyy-mm-dd" value="{{ old('invoice_date') }}">
      @if ($errors->has('invoice_date'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('invoice_date') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="payment_terms" class="col-md-4 col-form-label text-md-right">Tanggal Pembayaran</label>
    <div class="col-md-6">
      <input type="date" class="form-control {{ $errors->has('payment_terms') ? 'is-invalid' : '' }}" name="payment_terms" id="payment_terms" placeholder="yyyy-mm-dd" value="{{ old('payment_terms') }}">
      @if ($errors->has('payment_terms'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('payment_terms') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="department" class="col-md-4 col-form-label text-md-right">Departemen</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('department') ? 'is-invalid' : '' }}" name="department" id="department" placeholder="Ex : Logistik" value="{{ old('department') }}">
      @if ($errors->has('department'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('department') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop