@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('invoice-detail', $invoice) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('order'))
  @component('layouts.components.alert', [
    'message' => $errors->first('order'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('product'))
  @component('layouts.components.alert', [
    'message' => $errors->first('product'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('qty'))
  @component('layouts.components.alert', [
    'message' => $errors->first('qty'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-6">
    <h3>Detil Invoice</h3>
  </div>
  <div class="col-6">
    <a class="btn btn-outline-secondary float-right" href="{{ url('invoice/'.$invoice->id.'/edit') }}" style="margin: 0 5px">
      <span class="fas fa-edit text-center">&nbsp Ubah Invoice</span>
    </a>
  </div>
</div>

<br/>

<div class="row">
  <label class="col-md-3 col-6 col-form-label text-right"><b>Nomor Invoice</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $invoice->invoice_number }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Nomor Order</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $invoice->order->order_number }}</label>

  <label class="col-md-3 col-6 col-form-label text-right"><b>Customer </b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $invoice->order->customer->name }}</label>

  <label class="col-md-3 col-6 col-form-label text-right"><b>Alamat</b></label>
  <label class="col-md-9 col-6 col-form-label">
    @if (!empty($invoice->order->address))
      {{ $invoice->order->address }}
    @else
      {{ $invoice->customer->address }}
    @endif
  </label>
    
  <label class="col-md-3 col-6 col-form-label text-right"><b>Tanggal Order</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ date('d F Y', strtotime($invoice->order->order_date)) }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Tanggal Invoice</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ date('d F Y', strtotime($invoice->invoice_date)) }}</label>

  <label class="col-md-3 col-6 col-form-label text-right"><b>Tanggal Pembayaran</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ date('d F Y', strtotime($invoice->payment_terms)) }}</label>
</div>

<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>Produk</b></span></th>
      <th class="text-center"><span><b>Keterangan</b></span></th>
      <th class="text-center"><span><b>Jumlah</b></span></th>
      <th class="text-center"><span><b>Harga</b></span></th>
    </thead>
    <tbody>
      @if ($order->lineItems->isEmpty())
        <tr>
          <td colspan="5" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php $i = 0 @endphp
        @foreach ($order->lineItems as $item)
          <tr>
            <td class="text-center">{{ ++$i }}.</td>
            <td>{{ ucwords(strtolower($item->product->name)) }}</td>
            <td>@if (!empty($item->variant)) {{ ucwords(strtolower($item->variant->name)) }} @endif</td>
            <td class="text-center">{{ $item->qty }} {{ $item->unit }}</td>
            <td class="text-right">IDR {{ number_format($item->price, 2, ',', '.') }}</td>
          </tr>
        @endforeach
      @endif
      <tr>
        <td class="text-right" colspan="4"><b>Subtotal</b></td>
        <td class="text-right">IDR {{ number_format($order->subtotal, 2, ',', '.') }}</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Diskon</b></td>
        <td class="text-right">IDR {{ number_format($order->discount, 2, ',', '.') }}</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>PPN @if (!empty($order->vat)) ({{ $order->vat }}%) @endif</b></td>
        <td class="text-right">IDR {{ number_format($order->tax, 2, ',', '.') }}</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Diskon</b></td>
        <td class="text-right">IDR {{ number_format($order->discount, 2, ',', '.') }}</td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Grand Total</b></td>
        <td class="text-right"><b>IDR {{ number_format($order->total, 2, ',', '.') }}</b></td>
      </tr>
    </tbody>
  </table>
</div>

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop