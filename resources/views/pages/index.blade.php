@extends('layouts.app')

@section('content')
<h1>Selamat datang {{ ucwords(strtolower(Auth::user()->name)) }}</h1>

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<br/>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-user text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Manajemen Pengguna</h5>
        <a href="{{ url('user') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-user-tie text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Customer</h5>
        <a href="{{ url('customer') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-people-carry text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Supplier</h5>
        <a href="{{ url('supplier') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-file-alt text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Produk</h5>
        <a href="{{ url('product') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fa fa-download text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Template</h5>
        <a href="{{ url('doc/template.xlsx') }}" class="btn btn-secondary btn-block">Download</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fa fa-upload text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">File</h5>
        <form method="post" action="{{ url('upload') }}" enctype="multipart/form-data" id="uploadForm">
          {{ csrf_field() }}
          <input type="file" id="upload" name="upload" style="display:none">
          <input type="button" for="upload" value="Upload" class="btn btn-secondary btn-block" onclick="document.getElementById('upload').click()">
          <script>
            document.getElementById("upload").onchange = function() {
              document.getElementById("uploadForm").submit();
            };
            onclick="var f=document.createElement('input');f.style.display='none';f.type='file';f.name='file';document.getElementById('uploadForm').appendChild(f);f.click();"
          </script>
        </form>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-shopping-cart text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Order</h5>
        <a href="{{ url('order') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-envelope-open-text text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Surat Jalan</h5>
        <a href="{{ url('delivery') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-file-invoice text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Invoice</h5>
        <a href="{{ url('invoice') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-exchange-alt text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Kas</h5>
        <a href="{{ url('cash') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fa fa-file text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Laporan</h5>
        <a href="{{ url('report') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
</div>
@stop
