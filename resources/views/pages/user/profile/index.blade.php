@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('profile') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-3">
    <h1>Profil</h1>
  </div>
  <div class="col-9">
    <a href="{{ url('user/profile/'.$item->id.'/password') }}" class="btn btn-secondary" style="float: right; margin-left: 10px">Ganti Password</a>
    <a href="{{ url('user/profile/'.$item->id.'/edit') }}" class="btn btn-secondary" style="float: right; margin-left: 10px">Ubah Profil</a>
  </div>
</div>

<div class="table-responsive">
  <table class="table table-bordered">
    <tbody>
      <tr>
        <td style="width: 30%; background-color: #f5f5f5">Nama</td>
        <td style="width: 70%">{{ ucwords(strtolower($item->name)) }}</td>
      </tr>
      <tr>
        <td style="width: 30%; background-color: #f5f5f5">Username</td>
        <td style="width: 70%">{{ strtolower($item->username) }}</td>
      </tr>
      <tr>
        <td style="width: 30%; background-color: #f5f5f5">Role</td>
        <td style="width: 70%">
          @foreach ($item->getRoleNames() as $role)
            {{ ucwords(strtolower($role)) }}
          @endforeach
        </td>
      </tr>
      <tr>
        <td style="width: 30%; background-color: #f5f5f5">Permission</td>
        <td style="width: 70%">
          <ul>
            @foreach ($item->getPermissionsViaRoles() as $permission)
              <li>{{ ucwords(strtolower($permission->name)) }}</li>
            @endforeach
          </ul>
        </td>
      </tr>
    </tbody>
  </table>
</div>
@stop