@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('profile-password', $profile) }}

@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<h1 class="text-center">Ganti Password</h1>

<br/>

<form method="post" action="{{ url('user/profile/'.$profile->id.'/password/update') }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label for="old_password" class="col-md-4 col-form-label text-md-right">Password Lama</label>
    <div class="col-md-6">
      <input type="password" class="form-control {{ $errors->has('old_password') ? 'is-invalid' : '' }}" name="old_password" id="old_password">
      @if ($errors->has('old_password'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('old_password') }}</strong>
				</span>
			@endif
    </div>
  </div>
  <div class="form-group row">
		<label for="password" class="col-md-4 col-form-label text-md-right">Password Baru</label>
		<div class="col-md-6">
			<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
			@if ($errors->has('password'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
			@endif
		</div>
	</div>
  <div class="form-group row">
		<label for="password-confirm" class="col-md-4 col-form-label text-md-right">Konfirmasi Password Baru</label>
		<div class="col-md-6">
			<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
		</div>
	</div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Ubah</button>
      <a href="{{ url('user/profile') }}" class="btn btn-danger">Batal</a>
    </div>
  </div>
</form>
@stop
