@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('member') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('name'))
  @component('layouts.components.alert', [
    'message' => $errors->first('name'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('username'))
  @component('layouts.components.alert', [
    'message' => $errors->first('username'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('role'))
  @component('layouts.components.alert', [
    'message' => $errors->first('role'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Member</h1>
  </div>
</div>

<br/>

<form method="post" action="{{ url('user/member') }}">
  {{ csrf_field() }}
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Nama</span>
    </div>
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
    <div class="input-group-prepend">
      <span class="input-group-text">Username</span>
    </div>
    <input type="text" class="form-control" name="username" id="username" value="{{ old('username') }}">
    <div class="input-group-prepend">
      <span class="input-group-text">Role</span>
    </div>
    <select class="form-control" name="role" id="role">
      <option disabled @if (empty(old('role'))) selected @endif>Pilih Role</option>
      @foreach ($roles as $item)
      <option value="{{ $item->id }}" @if (old('role') == $item->id) selected @endif>{{ ucwords(strtolower($item->name)) }}</option>
      @endforeach
    </select>
    <div class="input-group-prepend">
      <button class="btn btn-outline-secondary" type="submit">
        <span class="fas fa-plus text-center"></span>
      </button>
    </div>
  </div>
</form>

<br/>

<!-- Sort & Filter -->
@component('layouts.components.sortfilter', [
  'path' => 'user/member',
  'show' => $show,
  'search' => $search,
  'createPath' => null
]) @endcomponent

<!-- Table -->
<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <tr>
        <form method="get" action="{{ url('user/member') }}">
          {{ csrf_field() }}
          <input type="hidden" name="show" id="show" value="{{ $show }}">
          <input type="hidden" name="search" id="search" value="{{ $search }}">
          <input type="hidden" name="sort" id="sort" value="">
          <input type="hidden" name="page" id="page" value="{{ $page }}">
          <th class="text-center">
            <button
              class="btn btn-block btn-primary-outline no-click"
              id="id">
              <span><b>No.</b></span>
            </button>
          </th>
          <th class="text-center">
            <button
              class="btn btn-block btn-primary-outline"
              id="name"
              onclick="setupSort('{{ ($sort == "u.name,asc") ? "u.name,desc" : "u.name,asc" }}')">
              <span class="float-left"><b>Nama</b></span>
              <span style="padding-top: 4px" class="float-right fa @if ($sort == 'u.name,asc') fa-sort-up @elseif ($sort == 'u.name,desc') fa-sort-down @else fa-sort @endif"></span>
            </button>
          </th>
          <th class="text-center">
            <button
              class="btn btn-block btn-primary-outline"
              id="username"
              onclick="setupSort('{{ ($sort == "u.username,asc") ? "u.username,desc" : "u.username,asc" }}')">
              <span class="float-left"><b>Username</b></span>
              <span style="padding-top: 4px" class="float-right fa @if ($sort == 'u.username,asc') fa-sort-up @elseif ($sort == 'u.username,desc') fa-sort-down @else fa-sort @endif"></span>
            </button>
          </th>
          <th class="text-center">
            <button
              class="btn btn-block btn-primary-outline"
              id="role"
              onclick="setupSort('{{ ($sort == "r.name,asc") ? "r.name,desc" : "r.name,asc" }}')">
              <span class="float-left"><b>Role</b></span>
              <span style="padding-top: 4px" class="float-right fa @if ($sort == 'r.name,asc') fa-sort-up @elseif ($sort == 'r.name,desc') fa-sort-down @else fa-sort @endif"></span>
            </button>
          </th>
          <th></th>
        </form>
      </tr>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="5" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @endif
      @foreach ($items as $item)
        <tr>
          <td class="text-center">{{ ++$iterator }}.</td>
          <td>{{ ucwords(strtolower($item->name)) }}</td>
          <td>{{ $item->username }}</td>
          <td>
            @php $user = true @endphp
            @foreach ($item->getRoleNames() as $role)
              {{ ucwords(strtolower($role)) }}
              @if ($role == 'super admin' || $role == 'admin')
                @php $user = false @endphp
              @endif
            @endforeach
          </td>
          <td class="text-center">
            @if ($user)
              <a href="{{ url('user/member/'.$item->id.'/edit') }}" class="btn btn-warning" style="color: white">
                <span class="fas fa-edit" title="update"></span>
              </a>
              <button class="btn btn-danger open-confirm" data-name="{{ ucwords(strtolower($item->name)) }}" data-url="{{ url('user/member/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

<!-- Pagination -->
@component('layouts.components.pagination', [
  'path' => 'user/member',
  'items' => $items,
  'token' => $token,
  'show' => $show,
  'search' => $search,
  'sort' => $sort
]) @endcomponent

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop