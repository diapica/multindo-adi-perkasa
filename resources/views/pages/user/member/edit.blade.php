@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('member-edit', $member) }}

@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<h1 class="text-center">Ubah Pengguna</h1>

<br/>

<form method="post" action="{{ url('user/member/'.$member->id) }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">Nama</label>
    <div class="col-md-5">
      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" id="name" value="{{ ucwords(strtolower($member->name)) }}"> 
      @if ($errors->has('name'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
    </div>
  </div>
  <div class="form-group row">
    <label for="username" class="col-md-4 col-form-label text-md-right">Username</label>
    <div class="col-md-5">
      <input type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" id="username" value="{{ strtolower($member->username) }}">
      @if ($errors->has('username'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('username') }}</strong>
				</span>
			@endif
    </div>
  </div>
  <div class="form-group row">
    <label for="role" class="col-md-4 col-form-label text-md-right">Role</label>
    <div class="col-md-5">
      <select class="form-control {{ $errors->has('role') ? 'is-invalid' : '' }}" name="role" id="role" value="{{ $member->role_id }}">
        <option disabled @if (empty($member->role_id)) selected @endif>Pilih Role</option>
        @foreach ($roles as $item)
        <option value="{{ $item->id }}" @if ($member->role_id == $item->id) selected @endif>{{ ucwords(strtolower($item->name)) }}</option>
        @endforeach
      </select>
      @if ($errors->has('role'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('role') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop