@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('user') }}

<br/>

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-id-card text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Profil</h5>
        <a href="{{ url('user/profile') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  {{-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-user-shield text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Permission</h5>
        <a href="{{ url('user/permission') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div> --}}
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-user-cog text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Role</h5>
        <a href="{{ url('user/role') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-users text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Member</h5>
        <a href="{{ url('user/member') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
</div>
@stop
