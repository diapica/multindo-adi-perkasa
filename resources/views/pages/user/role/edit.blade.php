@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('role-edit', $role) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<h1 class="text-center">Ubah Role</h1>

<br/>

<form method="post" action="{{ url('user/role/'.$role->id) }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">Nama</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" id="name" value="{{ ucwords(strtolower($role->name)) }}">
      @if ($errors->has('name'))
				<span class="invalid-feedback">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
    </div>
  </div>
  <div class="form-group row">
    <label for="permission" class="col-md-4 col-form-label text-md-right">Permission</label>
    <div class="col-md-6">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" onClick="checkAll(this, 'permission[]')">Check All
      </div>
      @foreach ($permissions as $permission)
        <div class="form-check">
          <input class="form-check-input" type="checkbox" name="permission[]" value="{{ $permission->id }}"
            @foreach ($role->getPermissionNames() as $rolePermission)
              @if ($permission->name == $rolePermission) checked @endif
            @endforeach>
          <label class="form-check-label">{{ ucwords(strtolower($permission->name)) }}</label>
        </div>
      @endforeach
      @if ($errors->has('permission'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('permission') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop