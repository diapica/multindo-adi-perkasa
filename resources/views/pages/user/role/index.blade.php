@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('role') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('name'))
  @component('layouts.components.alert', [
    'message' => $errors->first('name'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Role</h1>
  </div>
</div>

<br/>

<form method="post" action="{{ url('user/role') }}">
  {{ csrf_field() }}
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Nama</span>
    </div>
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
    <div class="input-group-prepend">
      <button class="btn btn-outline-secondary" type="submit">
        <span class="fas fa-plus text-center"></span>
      </button>
    </div>
  </div>
</form>

<br/>

<!-- Sort & Filter -->
@component('layouts.components.sortfilter', [
  'path' => 'user/role',
  'show' => $show,
  'search' => $search,
  'createPath' => null
]) @endcomponent

<!-- Table -->
<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <tr>
        <form method="get" action="{{ url('user/role') }}">
          {{ csrf_field() }}
          <input type="hidden" name="show" id="show" value="{{ $show }}">
          <input type="hidden" name="search" id="search" value="{{ $search }}">
          <input type="hidden" name="sort" id="sort" value="">
          <input type="hidden" name="page" id="page" value="{{ $page }}">
          <th class="text-center">
            <button
              class="btn btn-block btn-primary-outline no-click"
              id="id">
              <span><b>No.</b></span>
            </button>
          </th>
          <th class="text-center">
            <button
              class="btn btn-block btn-primary-outline"
              id="name"
              onclick="setupSort('{{ ($sort == "name,asc") ? "name,desc" : "name,asc" }}')">
              <span class="float-left"><b>Nama</b></span>
              <span style="padding-top: 4px" class="float-right fa @if ($sort == 'name,asc') fa-sort-up @elseif ($sort == 'name,desc') fa-sort-down @else fa-sort @endif"></span>
            </button>
          </th>
          <th class="text-center">
            <button
              class="btn btn-block btn-primary-outline no-click"
              id="permission">
              <span class="float-left"><b>Permission</b></span>
            </button>
          </th>
          <th></th>
        </form>
      </tr>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="4" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @endif
      @foreach ($items as $item)
        <tr>
          <td class="text-center">{{ ++$iterator }}.</td>
          <td>{{ ucwords(strtolower($item->name)) }}</td>
          <td>
            <ul>
              @foreach ($item->getPermissionNames() as $permission)
                <li>{{ ucwords(strtolower($permission)) }}</li>
              @endforeach
            </ul>
          </td>
          <td class="text-center">
            <a href="{{ url('user/role/'.$item->id.'/edit') }}" class="btn btn-warning" style="color: white">
              <span class="fas fa-edit" title="update"></span>
            </a>
            <button class="btn btn-danger open-confirm" data-name="{{ ucwords(strtolower($item->name)) }}" data-url="{{ url('user/role/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
              <span class="fas fa-trash" title="delete"></span>
            </button>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>

<!-- Pagination -->
@component('layouts.components.pagination', [
  'path' => 'user/role',
  'items' => $items,
  'token' => $token,
  'show' => $show,
  'search' => $search,
  'sort' => $sort
]) @endcomponent

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop
