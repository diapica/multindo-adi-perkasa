@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render($breadcrumbs) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<form method="get" action="{{ url('report/'.$type) }}">
  {{ csrf_field() }}
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Awal</span>
    </div>
    <input type="date" class="form-control" name="since" id="since" autofocus @if (!empty($since)) value="{{ $since }}" @endif>
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Akhir</span>
    </div>
    <input type="date" class="form-control" name="until" id="until" @if (!empty($until)) value="{{ $until }}" @endif>
    <div class="input-group-prepend">
      <button class="btn btn-outline-secondary" type="submit">Submit</button>
    </div>
  </div>
</form>

<br/>
<h5>Total Order Masuk : {{ count($items) }}</h5>
<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>Tanggal Order</b></span></th>
      <th class="text-center"><span><b>No. Order</b></span></th>
      <th class="text-center"><span><b>Subtotal</b></span></th>
      <th class="text-center" colspan="2"><span><b>PPN</b></span></th>
      <th class="text-center"><span><b>Diskon</b></span></th>
      <th class="text-center"><span><b>Grand Total</b></span></th>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="8" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php $total = 0; $i = 0 @endphp
        @foreach ($items as $item)
          @if ($item->order_status != "CANCELLED")
            <tr>
              <td class="text-center">{{ ++$i }}.</td>
              <td>{{ date('d F Y', strtotime($item->order_date)) }}</td>
              <td>
                <a target="_blank"
                  @if ($type=='order-out')
                    href="{{ url('order/'.$item->parent->id.'/out') }}"
                  @else
                    href="{{ url('order/'.$item->id) }}"
                  @endif>
                  {{ strtoupper($item->order_number) }}
                </a>
              </td>
              <td>IDR {{ number_format($item->subtotal, 2, ',', '.') }}</td>
              <td>@if (!empty($item->vat)) {{ $item->vat }}% @endif</td>
              <td>IDR {{ number_format($item->tax, 2, ',', '.') }}</td>
              <td>IDR {{ number_format($item->discount, 2, ',', '.') }}</td>
              <td>IDR {{ number_format($item->total, 2, ',', '.') }}</td>
              @php $total += $item->total @endphp
            </tr>
          @endif
        @endforeach
        <tr>
          <td class="text-right" colspan="7"><b>Total</b></td>
          <td>IDR {{ number_format($total, 2, ',', '.') }}</td>
        </tr>
      @endif
    </tbody>
  </table>
</div>
@stop