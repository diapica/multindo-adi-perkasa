@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render($breadcrumbs) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<form method="get" action="{{ url('report/'.$type) }}">
  {{ csrf_field() }}
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Awal</span>
    </div>
    <input type="date" class="form-control" name="since" id="since" autofocus @if (!empty($since)) value="{{ $since }}" @endif>
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Akhir</span>
    </div>
    <input type="date" class="form-control" name="until" id="until" @if (!empty($until)) value="{{ $until }}" @endif>
    <div class="input-group-prepend">
      <button class="btn btn-outline-secondary" type="submit">Submit</button>
    </div>
  </div>
</form>

<br/>
<h5>Total Transaksi : {{ count($items) }}</h5>
<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>Tanggal Transaksi</b></span></th>
      <th class="text-center"><span><b>Keterangan</b></span></th>
      <th class="text-center"><span><b>Tipe Transaksi</b></span></th>
      <th class="text-center"><span><b>Pemasukan </b></span></th>
      <th class="text-center"><span><b>Pengeluaran</b></span></th>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="7" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php
          $totalIncome = 0;
          $totalExpense = 0;
          $i = 0;
        @endphp
        @foreach ($items as $item)
          <tr>
            <td class="text-center">{{ ++$i }}.</td>
            <td class="text-center">{{ date('d F Y', strtotime($item->transaction_date)) }}</td>
            <td class="text-center">{{ $item->description }}</td>
            <td class="text-center">@if ($item->type == 'EXPENSE') Pengeluaran @else Pemasukan @endif</td>
            <td class="text-right">@if (!empty($item->income)) + IDR {{ number_format($item->income, 2, ',', '.') }} @endif</td>
            <td class="text-right">@if (!empty($item->expense)) - IDR {{ number_format($item->expense, 2, ',', '.') }} @endif</td>
          </tr>
          @php
            $totalIncome += $item->income;
            $totalExpense += $item->expense;
          @endphp
        @endforeach
        <tr>
          <th colspan=4 class="text-right"><b>Total</b></th>
          <th colspan=2 class="text-right"><b>IDR {{ number_format(($totalIncome - $totalExpense), 2, ',', '.') }}</b></th>
        <tr>
      @endif
    </tbody>
  </table>
</div>
@stop