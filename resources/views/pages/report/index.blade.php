@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('report') }}

<br/>

<div class="row">

  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-list text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Order Masuk</h5>
        <a href="{{ url('report/order-in') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-list text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Order Keluar</h5>
        <a href="{{ url('report/order-out') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-list text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Kas</h5>
        <a href="{{ url('report/cash') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fas fa-list text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Surat Jalan</h5>
        <a href="{{ url('report/delivery') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
    <div class="card" style="width: 18rem; margin: 10px auto">
      <i class="fa fa-list text-center" style="font-size: 40pt; margin-top: 20px"></i>
      <div class="card-body">
        <h5 class="card-title text-center">Keuntungan</h5>
        <a href="{{ url('report/profit') }}" class="btn btn-secondary btn-block">Buka</a>
      </div>
    </div>
  </div>
</div>
@stop
