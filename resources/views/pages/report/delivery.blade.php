@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render($breadcrumbs) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<form method="get" action="{{ url('report/'.$type) }}">
  {{ csrf_field() }}
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Awal</span>
    </div>
    <input type="date" class="form-control" name="since" id="since" autofocus @if (!empty($since)) value="{{ $since }}" @endif>
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Akhir</span>
    </div>
    <input type="date" class="form-control" name="until" id="until" @if (!empty($until)) value="{{ $until }}" @endif>
    <div class="input-group-prepend">
      <button class="btn btn-outline-secondary" type="submit">Submit</button>
    </div>
  </div>
</form>

<br/>
<h5>Total Surat Jalan : {{ count($items) }}</h5>
<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>No. Surat Jalan</b></span></th>
      <th class="text-center"><span><b>Customer</b></span></th>
      <th class="text-center"><span><b>Tanggal Pengiriman</b></span></th>
      <th class="text-center"><span><b>Alamat Pengiriman</b></span></th>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="5" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php $i = 0 @endphp
        @foreach ($items as $item)
          <tr>
            <td class="text-center">{{ ++$i }}.</td>
            <td class="text-center">
              <a target=_blank href="{{ url('delivery/'.$item->id) }}">
                {{ $item->delivery_number }}
              </a>
            </td>
            <td class="text-center">{{ $item->customer->name }}</td>
            <td class="text-center">{{ date('d F Y', strtotime($item->delivery_date)) }}</td>
            <td class="text-center">
              @if (!empty($item->address))
                {{ $item->address }}
              @else
                {{ $item->customer->address }}
              @endif
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
</div>
@stop