@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render($breadcrumbs) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<form method="get" action="{{ url('report/'.$type) }}">
  {{ csrf_field() }}
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Awal</span>
    </div>
    <input type="date" class="form-control" name="since" id="since" autofocus @if (!empty($since)) value="{{ $since }}" @endif>
    <div class="input-group-prepend">
      <span class="input-group-text">Tanggal Akhir</span>
    </div>
    <input type="date" class="form-control" name="until" id="until" @if (!empty($until)) value="{{ $until }}" @endif>
    <div class="input-group-prepend">
      <button class="btn btn-outline-secondary" type="submit">Submit</button>
    </div>
  </div>
</form>

<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>No. Order Masuk</b></span></th>
      <th class="text-center"><span><b>Total Order Masuk</b></span></th>
      <th class="text-center"><span><b>No. Order Keluar</b></span></th>
      <th class="text-center"><span><b>Total Order Keluar</b></span></th>
      <th class="text-center"><span><b>Keuntungan</b></span></th>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="6" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php
          $totalOrderIn = 0;
          $totalOrderOut = 0;
          $totalProfit = 0;
          $i = 0;
        @endphp
        @foreach ($items as $item)
          @if ($item->status == "COMPLETED")
            <tr>
              <td class="text-center">{{ ++$i }}.</td>
              <td>
                <a target="_blank" href="{{ url('order/'.$item->id) }}">
                  {{ strtoupper($item->order_number) }}
                </a>
              </td>
              <td>IDR {{ number_format($item->total, 2, ',', '.') }}</td>
              <td>
                <a target="_blank" href="{{ url('order/'.$item->id.'/out') }}">
                  {{ strtoupper($item->child->order_number) }}
                </a>
              </td>
              <td>IDR {{ number_format($item->child->total, 2, ',', '.') }}</td>
              <td>IDR {{ number_format(($item->total - $item->child->total), 2, ',', '.') }}</td>
            </tr>
            @php
              $totalOrderIn += $item->total;
              $totalOrderOut += $item->child->total;
              $totalProfit += ($item->total - $item->child->total);
            @endphp
          @endif
        @endforeach
        <tr>
          <th class="text-right" colspan="2">Total</th>
          <th>IDR {{ number_format($totalOrderIn, 2, ',', '.') }}</th>
          <th></th>
          <th>IDR {{ number_format($totalOrderOut, 2, ',', '.') }}</th>
          <th>IDR {{ number_format($totalProfit, 2, ',', '.') }}</th>
        </tr>
      @endif
    </tbody>
  </table>
</div>
@stop