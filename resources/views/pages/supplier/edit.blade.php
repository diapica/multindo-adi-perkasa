@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('supplier-edit', $supplier) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<h1 class="text-center">Ubah Supplier</h1>

<br/>

<form method="post" action="{{ url('supplier/'.$supplier->id) }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">Nama</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"  name="name" id="name" value="{{ $supplier->name }}">
      @if ($errors->has('name'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('name') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" id="email" value="{{ strtolower($supplier->email) }}">
      @if ($errors->has('email'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="address" class="col-md-4 col-form-label text-md-right">Alamat</label>
    <div class="col-md-6">
      <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{{ $supplier->address }}</textarea>
      @if ($errors->has('address'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('address') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="phone" class="col-md-4 col-form-label text-md-right">No. Telepon</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" name="phone" id="phone" value="{{ $supplier->phone }}">
      @if ($errors->has('phone'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('phone') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="fax" class="col-md-4 col-form-label text-md-right">Fax</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('fax') ? 'is-invalid' : '' }}" name="fax" id="fax" value="{{ $supplier->fax }}">
      @if ($errors->has('fax'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('fax') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop