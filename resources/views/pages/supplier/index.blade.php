@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('supplier') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Supplier</h1>
  </div>
</div>

<br/>

<!-- Sort & Filter -->
@component('layouts.components.sortfilter', [
  'path' => 'supplier',
  'show' => $show,
  'search' => $search,
  'createPath' => 'supplier/create'
]) @endcomponent

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <form method="get" action="{{ url('supplier') }}">
        {{ csrf_field() }}
        <input type="hidden" name="show" id="show" value="{{ $show }}">
        <input type="hidden" name="search" id="search" value="{{ $search }}">
        <input type="hidden" name="sort" id="sort" value="{{ $sort }}">
        <input type="hidden" name="page" id="page" value="{{ $page }}">
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="id">
            <span><b>No.</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="name"
            onclick="setupSort('{{ ($sort == "name,asc") ? "name,desc" : "name,asc" }}')">
            <span class="float-left"><b>Nama</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'name,asc') fa-sort-up @elseif ($sort == 'name,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="email"
            onclick="setupSort('{{ ($sort == "email,asc") ? "email,desc" : "email,asc" }}')">
            <span class="float-left"><b>E-Mail</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'email,asc') fa-sort-up @elseif ($sort == 'email,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="address">
            <span class="float-left"><b>Alamat</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="phone">
            <span class="float-left"><b>Nomor Telepon</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="fax">
            <span class="float-left"><b>Fax</b></span>
          </button>
        </th>
        <th></th>
      </form>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="7" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @foreach ($items as $item)
          <tr>
            <td class="text-center">{{ ++$iterator }}.</td>
            <td>{{ $item->name }}</td>
            <td class="text-right">{{ strtolower($item->email) }}</td>
            <td>{{ $item->address }}</td>
            <td class="text-right">{{ $item->phone }}</td>
            <td class="text-right">{{ $item->fax }}</td>
            <td class="text-center">
              <a href="{{ url('supplier/'.$item->id.'/edit') }}" class="btn btn-warning" style="color: white">
                <span class="fas fa-edit" title="update"></span>
              </a>
              <button class="btn btn-danger open-confirm" data-name="{{ $item->name }}" data-url="{{ url('supplier/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
</div>

<!-- Pagination -->
@component('layouts.components.pagination', [
  'path' => 'supplier',
  'items' => $items,
  'token' => $token,
  'show' => $show,
  'search' => $search,
  'sort' => $sort
]) @endcomponent

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop