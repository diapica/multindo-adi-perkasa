@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('product-edit', $product) }}

<h1 class="text-center">Ubah Produk</h1>

<br/>

<form method="post" action="{{ url('product/'.$product->id) }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">Nama</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"  name="name" id="name" value="{{ ucwords(strtolower($product->name)) }}">
      @if ($errors->has('name'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('name') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop