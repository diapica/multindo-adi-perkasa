<div class="modal fade" id="order-line-item-create" tabindex="-1" role="dialog" aria-labelledby="Add Modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Order Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" id="form-create" enctype='multipart/form-data'>
          {{ csrf_field() }}
          <input type="hidden" name="order" value="{{ $order->id }}">
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Produk</b></label>
            <div class="col-sm-9 float-right">
              <input list="products" name="product" id="product" value="{{ old('product') }}" style="width: 100%" autofocus>
              <datalist id="products">
                @foreach ($products as $item)
                  <option data-id="{{ $item->id }}" value="{{ ucwords(strtolower($item->name)) }}"></option>
                @endforeach
              </datalist>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Keterangan</b></label>
            <div class="col-sm-9 float-right">
              <input list="variants" name="variant" id="variant" value="{{ old('variant') }}" style="width: 100%">
              <datalist id="variants">
                @foreach ($variants as $item)
                  <option id="variantOpt" data-product-id="{{ $item->product_id }}" value="{{ ucwords(strtolower($item->name)) }}"></option>
                @endforeach
              </datalist>
              <script>
                $(document).ready(function() {
                  $('#product').blur(function() {
                    $('#variants').children('option').attr('disabled', true);
                    
                    if ($('#product').val() != '') {
                      $('#variant').prop('disabled', false);
                    } else {
                      $('#variant').prop('disabled', true);
                    }

                    var productID = $("#products option[value='" + $('#product').val() + "']").attr('data-id');
                    $("#variants option[data-product-id='" + productID + "']").attr('disabled', false);
                  });
                });
              </script>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Jumlah</b></label>
            <div class="col-sm-9 float-right">
              <input type="number" min="1" class="form-control" name="qty" id="qty" value="{{ old('qty') }}">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Satuan</b></label>
            <div class="col-sm-9 float-right">
              <input type="text" class="form-control" name="unit" id="unit" value="{{ old('unit') }}">
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Harga</b></label>
            <div class="col-sm-9 float-right">
              <input type="number" min="1" class="form-control" name="price" id="price" value="{{ old('price') }}">
            </div>
          </div>
          @if ($type == 'out')
            <div class="form-group row">
              <label class="control-label col-sm-3"><b>Deskripsi</b></label>
              <div class="col-sm-9 float-right">
                <textarea class="form-control" name="description" id="description">{{ old('description') }}</textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3"><b>Gambar</b></label>
              <div class="col-sm-9 float-right">
                <input type="file" class="form-control" name="image" id="image" value="{{ old('image') }}">
              </div>
            </div>
          @endif
          <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>