@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('order-out-edit', $order) }}

<h1 class="text-center">Ubah Order</h1>

<br/>

<form method="post" action="{{ url('order/'.$order->id.'/out/'.$order->child->id) }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Supplier</label>
    <div class="col-md-6">
      <select class="form-control" name="supplier" id="supplier">
        <option disabled @if (empty($order->child->supplier_id)) selected @endif>Pilih Supplier</option>
        @foreach ($suppliers as $item)
          <option value="{{ $item->id }}" @if ($order->child->supplier_id == $item->id) selected @endif>{{ ucwords(strtolower($item->name)) }}</option>
        @endforeach
      </select>
      @if ($errors->has('supplier'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('supplier') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Nomor Order</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('order_number') ? 'is-invalid' : '' }}"  name="order_number" id="order_number" value="{{ $order->child->order_number }}">
      @if ($errors->has('order_number'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('order_number') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Tanggal Order</label>
    <div class="col-md-6">
      <input type="date" class="form-control {{ $errors->has('order_date') ? 'is-invalid' : '' }}" name="order_date" id="order_date" value="{{ $order->child->order_date }}">
      @if ($errors->has('order_date'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('order_date') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Tanggal Pengiriman</label>
    <div class="col-md-6">
      <input type="date" class="form-control {{ $errors->has('shipping_date') ? 'is-invalid' : '' }}" name="shipping_date" id="shipping_date" value="{{ $order->child->shipping_date }}">
      @if ($errors->has('shipping_date'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('shipping_date') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Alamat</label>
    <div class="col-md-6">
      <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{{ $order->child->address }}</textarea>
      @if ($errors->has('address'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('address') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Tipe PPN</label>
    <div class="col-md-6">
      <div class="form-check">
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="tax_type" {{ ($order->child->vat == 0) ? 'checked' : '' }} value="value">Value
        </label>
        <br/>
        <label class="form-check-label">
          <input type="radio" class="form-check-input" name="tax_type" {{ ($order->child->vat != 0) ? 'checked' : '' }} value="percentage">Percentage
        </label>
      </div>
    </div>
    <script>
      $(document).ready(function() {
        if ($('input[name=tax_type]').click(function() {
          $('#tax').val('');
          var type = $(this).val();
          switch (type) {
            case 'value':
              $('#tax').removeAttr('max');
              $('#tax').attr('placeholder', 100000);
              break;
            case 'percentage':
              $('#tax').attr('max', 100);
              $('#tax').attr('placeholder', 10);
              break;
          }
        }));
      });
    </script>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">PPN</label>
    <div class="col-md-6">
      <input type="number" class="form-control {{ $errors->has('tax') ? 'is-invalid' : '' }}" name="tax" id="tax" min="0" placeholder="10"
        @if ($order->child->vat != 0) value={{ $order->child->vat }} max="100"
        @else value={{ $order->child->tax }} @endif>
      @if ($errors->has('tax'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('tax') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Diskon</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('discount') ? 'is-invalid' : '' }}" name="discount" id="discount" value="{{ $order->child->discount }}" placeholder="100000">
      @if ($errors->has('discount'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('discount') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Status</label>
    <div class="col-md-6">
      <select class="form-control" name="status" id="status">
        @php $bool = true @endphp
        @foreach ($status as $item)
          @if ($order->child->status == $item)
            @php $bool = false @endphp
          @endif
          <option value="{{ $item }}" @if ($order->status == $item) selected @endif>{{ ucwords(strtolower($item)) }}</option>
        @endforeach
        @if ($bool)
          <option value="{{ $order->child->status }}" selected>{{ ucwords(strtolower($order->child->status)) }}</option>
        @endif
      </select>
      @if ($errors->has('status'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('status') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop