@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('order-out', $order) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-6">
    <h3>PO Keluar</h3>
  </div>
  <div class="col-6">
    <button class="btn btn-outline-secondary open-create float-right" data-url="{{ url('order/'.$order->id.'/out/'.$order->child->id) }}" data-toggle="modal" data-target="#order-line-item-create" style="margin: 0 5px">
      <span class="fas fa-plus text-center">&nbsp Order Item</span>
    </button>
    <a class="btn btn-outline-secondary float-right" href="{{ url('order/'.$order->id.'/out/edit') }}" style="margin: 0 5px">
      <span class="fas fa-edit text-center">&nbsp Ubah Order</span>
    </a>
  </div>
</div>

<!-- Order Line Item Create Modal -->
@component('pages.order.components.order-line-item', [
  'order' => $order->child,
  'products' => $products,
  'variants' => $variants,
  'type' => 'out'
]) @endcomponent

<br>

<div class="row">
  <label class="col-md-3 col-6 col-form-label text-right"><b>Nomor Order</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $order->child->order_number }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Tanggal Order</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ date('d F Y',strtotime($order->child->order_date)) }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Supplier</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $order->child->supplier->name }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Tanggal Pengiriman</b></label>
  <label class="col-md-9 col-6 col-form-label">
    @if (!empty($order->child->shipping_date))
      {{ date('d F Y',strtotime($order->child->shipping_date)) }}
    @else
      Belum ditentukan.
    @endif
  </label>

  <label class="col-md-3 col-6 col-form-label text-right"><b>Alamat</b></label>
  <label class="col-md-9 col-6 col-form-label">
    @if (!empty($order->child->address))
      {{ ucwords(strtolower($order->child->address)) }}
    @else
      Belum ditentukan.
    @endif
  </label>

  <label class="col-md-3 col-6 col-form-label text-right"><b>Status</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ ucwords(strtolower($order->child->status)) }}</label>
</div>

<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>Produk</b></span></th>
      <th class="text-center"><span><b>Keterangan</b></span></th>
      <th class="text-center"><span><b>Jumlah</b></span></th>
      <th class="text-center"><span><b>Gambar</b></span></th>
      <th class="text-center"><span><b>Deskripsi</b></span></th>
      <th class="text-center"><span><b>Harga</b></span></th>
      <th></th>
    </thead>
    <tbody>
      @if ($order->child->lineItems->isEmpty())
        <tr>
          <td colspan="8" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php $i = 0 @endphp
        @foreach ($order->child->lineItems as $item)
          <tr>
            <td class="text-center">{{ ++$i }}.</td>
            <td>{{ ucwords(strtolower($item->product->name)) }}</td>
            <td>@if (!empty($item->variant)) {{ ucwords(strtolower($item->variant->name)) }} @endif</td>
            <td class="text-center">{{ $item->qty }} {{ $item->unit }}</td>
            <td>
              @if (!empty($item->image))
                <img height=100 widht=100 src="{{ asset(env('ORDER_IMAGE_PATH').'/'.$order->id.'/out/'.$item->id) }}"></img>
              @endif
            </td>
            <td>{{ ucwords(strtolower($item->description)) }}</td>
            <td class="text-right">IDR {{ number_format($item->price, 2, ',', '.') }}</td>
            <td class="text-center">
              <button class="btn btn-danger open-confirm" data-name="{{ ucwords(strtolower($item->product->name)) }}" data-url="{{ url('order/'.$order->id.'/out/'.$order->child->id.'/item/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
      <tr>
        <td class="text-right" colspan="6"><b>Subtotal</b></td>
        <td class="text-right">IDR {{ number_format($order->child->subtotal, 2, ',', '.') }}</td>
        <td></td>
      </tr>
      <tr>
        <td class="text-right" colspan="6"><b>PPN @if (!empty($order->child->vat)) ({{ $order->child->vat }}%) @endif</b></td>
        <td class="text-right">IDR {{ number_format($order->child->tax, 2, ',', '.') }}</td>
        <td></td>
      </tr>
      <tr>
        <td class="text-right" colspan="6"><b>Diskon</b></td>
        <td class="text-right">IDR {{ number_format($order->child->discount, 2, ',', '.') }}</td>
        <td></td>
      </tr>
      <tr>
        <td class="text-right" colspan="6"><b>Grand Total</b></td>
        <td class="text-right"><b>IDR {{ number_format($order->child->total, 2, ',', '.') }}</b></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent

@stop