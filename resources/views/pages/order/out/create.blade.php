@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('order-out-create', $order) }}

<h1 class="text-center">Tambah PO Keluar</h1>

<br/>

<form method="post" action="{{ url('order/'.$order->id.'/out') }}">
  {{ csrf_field() }}
  <div class="form-group row">
    <label for="supplier" class="col-md-4 col-form-label text-md-right">Supplier</label>
    <div class="col-md-6">
      <select class="form-control" name="supplier" id="supplier">
        <option disabled @if (empty(old('supplier'))) selected @endif>Pilih Supplier</option>
        @foreach ($suppliers as $item)
          <option value="{{ $item->id }}" @if (old('supplier') == $item->id) selected @endif>{{ ucwords(strtolower($item->name)) }}</option>
        @endforeach
      </select>
      @if ($errors->has('supplier'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('supplier') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="order_number" class="col-md-4 col-form-label text-md-right">Nomor Order</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('order_number') ? 'is-invalid' : '' }}" name="order_number" id="order_number" placeholder="Ex : 001/AD/DPO/1/19" value="{{ old('order_number') }}">
      @if ($errors->has('order_number'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('order_number') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="order_date" class="col-md-4 col-form-label text-md-right">Tanggal Order</label>
    <div class="col-md-6">
      <input type="date" class="form-control {{ $errors->has('order_date') ? 'is-invalid' : '' }}" name="order_date" id="order_date" placeholder="yyyy-mm-dd">
      @if ($errors->has('order_date'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('order_date') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="address" class="col-md-4 col-form-label text-md-right">Alamat</label>
    <div class="col-md-6">
      <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{{ old('address') }}</textarea>
      @if ($errors->has('address'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('address') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop