@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('order-detail', $order) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('product'))
  @component('layouts.components.alert', [
    'message' => $errors->first('product'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('variant'))
  @component('layouts.components.alert', [
    'message' => $errors->first('variant'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('qty'))
  @component('layouts.components.alert', [
    'message' => $errors->first('qty'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('unit'))
  @component('layouts.components.alert', [
    'message' => $errors->first('unit'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('price'))
  @component('layouts.components.alert', [
    'message' => $errors->first('price'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-6">
    <h3>Detil Order</h3>
  </div>
  <div class="col-6">
    <button class="btn btn-outline-secondary open-create float-right" data-url="{{ url('order/'.$order->id) }}" data-toggle="modal" data-target="#order-line-item-create" style="margin: 0 5px">
      <span class="fas fa-plus text-center">&nbsp Order Item</span>
    </button>
    <a class="btn btn-outline-secondary float-right" href="{{ url('order/'.$order->id.'/edit') }}" style="margin: 0 5px">
      <span class="fas fa-edit text-center">&nbsp Ubah Order</span>
    </a>
    <a class="btn btn-outline-secondary float-right" href="{{ url('order/'.$order->id.'/out') }}" style="margin: 0 5px">
      <span class="fas fa-search text-center">&nbsp PO Keluar</span>
    </a>
  </div>
</div>

<!-- Order Line Item Create Modal -->
@component('pages.order.components.order-line-item', [
  'order' => $order,
  'products' => $products,
  'variants' => $variants,
  'type' => 'in'
]) @endcomponent

<br/>

<div class="row">
  <label class="col-md-3 col-6 col-form-label text-right"><b>Nomor Order</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $order->order_number }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Order Date</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ date('d F Y', strtotime($order->order_date)) }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Customer</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $order->customer->name }}</label>
    
  <label class="col-md-3 col-6 col-form-label text-right"><b>Alamat</b></label>
  <label class="col-md-9 col-6 col-form-label">
    @if (!empty($order->address))
      {{ ucwords(strtolower($order->address)) }}
    @else
      {{ ucwords(strtolower($order->customer->address)) }}
    @endif
  </label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Supplier</b></label>
  <label class="col-md-9 col-6 col-form-label">
    @if (!empty($order->child))
      {{ $order->child->supplier->name }}
    @else
      Belum ditentukan
    @endif
  </label>

  <label class="col-md-3 col-6 col-form-label text-right"><b>Status</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ ucwords(strtolower($order->status)) }}</label>
</div>

<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>Produk</b></span></th>
      <th class="text-center"><span><b>Keterangan</b></span></th>
      <th class="text-center"><span><b>Jumlah</b></span></th>
      <th class="text-center"><span><b>Harga</b></span></th>
      <th></th>
    </thead>
    <tbody>
      @if ($order->lineItems->isEmpty())
        <tr>
          <td colspan="7" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php $i = 0 @endphp
        @foreach ($order->lineItems as $item)
          <tr>
            <td class="text-center">{{ ++$i }}.</td>
            <td>{{ ucwords(strtolower($item->product->name)) }}</td>
            <td>@if (!empty($item->variant)) {{ ucwords(strtolower($item->variant->name)) }} @endif</td>
            <td class="text-center">{{ $item->qty }} {{ $item->unit }}</td>
            <td class="text-right">IDR {{ number_format($item->price, 2, ',', '.') }}</td>
            <td class="text-center">
              <button class="btn btn-danger open-confirm" data-name="{{ ucwords(strtolower($item->product->name)) }}" data-url="{{ url('order/'.$order->id.'/item/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
      <tr>
        <td class="text-right" colspan="4"><b>Subtotal</b></td>
        <td class="text-right">IDR {{ number_format($order->subtotal, 2, ',', '.') }}</td>
        <td></td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>PPN @if (!empty($order->vat)) ({{ $order->vat }}%) @endif</b></td>
        <td class="text-right">IDR {{ number_format($order->tax, 2, ',', '.') }}</td>
        <td></td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Diskon</b></td>
        <td class="text-right">IDR {{ number_format($order->discount, 2, ',', '.') }}</td>
        <td></td>
      </tr>
      <tr>
        <td class="text-right" colspan="4"><b>Grand Total</b></td>
        <td class="text-right"><b>IDR {{ number_format($order->total, 2, ',', '.') }}</b></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop