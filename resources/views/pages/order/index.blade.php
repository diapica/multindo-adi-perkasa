@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('order') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Order</h1>
  </div>
</div>

<br/>

<!-- Sort & Filter -->
@component('layouts.components.sortfilter', [
  'path' => 'order',
  'show' => $show,
  'search' => $search,
  'createPath' => 'order/create'
]) @endcomponent

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <form method="get" action="{{ url('order') }}">
        {{ csrf_field() }}
        <input type="hidden" name="show" id="show" value="{{ $show }}">
        <input type="hidden" name="search" id="search" value="{{ $search }}">
        <input type="hidden" name="sort" id="sort" value="{{ $sort }}">
        <input type="hidden" name="page" id="page" value="{{ $page }}">
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="id">
            <span><b>No.</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="order_number"
            onclick="setupSort('{{ ($sort == "order_number,asc") ? "order_number,desc" : "order_number,asc" }}')">
            <span><b>Nomor Order</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'order_number,asc') fa-sort-up @elseif ($sort == 'order_number,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="status"
            onclick="setupSort('{{ ($sort == "status,asc") ? "status,desc" : "status,asc" }}')">
            <span><b>Status</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'status,asc') fa-sort-up @elseif ($sort == 'status,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="customer">
            <span><b>Customer</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="total"
            onclick="setupSort('{{ ($sort == "total,asc") ? "total,desc" : "total,asc" }}')">
            <span><b>Total</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'total,asc') fa-sort-up @elseif ($sort == 'total,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th></th>
      </form>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="6" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @foreach ($items as $item)
          <tr>
            <td class="text-center">{{ ++$iterator }}.</td>
            <td>{{ $item->order_number }}</td>
            <td class="text-center">{{ ucwords(strtolower($item->status)) }}</td>
            <td>{{ $item->customer->name }}</td>
            <td class="text-right">IDR {{ number_format($item->total, 2, ',', '.') }}</td>
            <td class="text-center">
              <a href="{{ url('order/'.$item->id) }}" class="btn btn-primary" style="color: white">
                <span class="fas fa-search" title="show"></span>
              </a>
              <button class="btn btn-danger open-confirm" data-name="{{ $item->order_number }}" data-url="{{ url('order/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
</div>

<!-- Pagination -->
@component('layouts.components.pagination', [
  'path' => 'order',
  'items' => $items,
  'token' => $token,
  'show' => $show,
  'search' => $search,
  'sort' => $sort
]) @endcomponent

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop