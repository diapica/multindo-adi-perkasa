@extends('layouts.app')

@section('content')

<div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%)">
  <h4>Sorry, you're not eligible to access this page</h4><br/>
  <a class="btn btn-primary" style="width: 100%" href="{{ url()->previous() }}">Back</a>
</div>

@stop
