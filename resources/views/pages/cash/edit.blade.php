@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('cash-edit') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Ubah Kas</h1>
  </div>
</div>

<form method="post" action="{{ url('cash/'.$cash->id) }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label for="description" class="col-md-4 col-form-label text-md-right">Keterangan</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description" value="{{ ucwords(strtolower($cash->description)) }}">
      @if ($errors->has('description'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('description') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="type" class="col-md-4 col-form-label text-md-right">Tipe</label>
    <div class="col-md-6">
      <select class="form-control" name="type" id="type">
        <option value="expense"  @if($cash->type=='EXPENSE') selected @endif > Pengeluaran</option>
        <option value="income" @if($cash->type=='INCOME') selected @endif > Pemasukan</option>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label for="total" class="col-md-4 col-form-label text-md-right">Total</label>
    <div class="col-md-6">
      <input type="number" min="1" class="form-control {{ $errors->has('total') ? 'is-invalid' : '' }}"  name="total" id="total" @if($cash->type == 'EXPENSE') value="{{ $cash->expense }}" @else value="{{ $cash->income }}" @endif>
      @if ($errors->has('total'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('total') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop