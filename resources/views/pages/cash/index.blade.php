@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('cash') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('description'))
  @component('layouts.components.alert', [
    'message' => $errors->first('description'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('type'))
  @component('layouts.components.alert', [
    'message' => $errors->first('type'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('total'))
  @component('layouts.components.alert', [
    'message' => $errors->first('total'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Kas</h1>
  </div>
</div>

<form method="post" action="{{ url('cash') }}">
    {{ csrf_field() }}
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Keterangan</span>
    </div>
    <input type="text" class="form-control" name="description" id="description">
    <div class="input-group-prepend">
      <span class="input-group-text">Tipe</span>
    </div>
    <select class="custom-select" name="type" id="type">
      <option value="expense"  selected >Pengeluaran</option>
      <option value="income" >Pemasukan</option>
    </select>
    <div class="input-group-prepend">
      <span class="input-group-text">Total</span>
    </div>
    <input type="number" class="form-control" name="total" id="total" min="1">
    <div class="input-group-prepend">
      <button class="btn btn-outline-secondary" type="submit">
        <span class="fas fa-plus text-center"></span>
      </button>
    </div>
  </div>
</form>


<br/>

<!-- Sort & Filter -->
@component('layouts.components.sortfilter', [
  'path' => 'cash',
  'show' => $show,
  'search' => $search,
  'createPath' => null
]) @endcomponent

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <form method="get" action="{{ url('cash') }}">
        {{ csrf_field() }}
        <input type="hidden" name="show" id="show" value="{{ $show }}">
        <input type="hidden" name="search" id="search" value="{{ $search }}">
        <input type="hidden" name="sort" id="sort" value="{{ $sort }}">
        <input type="hidden" name="page" id="page" value="{{ $page }}">
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="id">
            <span><b>No.</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="transaction_date"
            onclick="setupSort('{{ ($sort == "transaction_date,asc") ? "transaction_date,desc" : "transaction_date,asc" }}')">
            <span><b>Tanggal Transaksi</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'transaction_date,asc') fa-sort-up @elseif ($sort == 'transaction_date,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="description"
            onclick="setupSort('{{ ($sort == "description,asc") ? "description,desc" : "description,asc" }}')">
            <span><b>Keterangan</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'description,asc') fa-sort-up @elseif ($sort == 'description,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="type"
            onclick="setupSort('{{ ($sort == "type,asc") ? "type,desc" : "type,asc" }}')">
            <span><b>Tipe</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'type,asc') fa-sort-up @elseif ($sort == 'type,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="income"
            onclick="setupSort('{{ ($sort == "income,asc") ? "income,desc" : "income,asc" }}')">
            <span><b>Pemasukan</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'income,asc') fa-sort-up @elseif ($sort == 'income,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="expense"
            onclick="setupSort('{{ ($sort == "expense,asc") ? "expense,desc" : "expense,asc" }}')">
            <span><b>Pengeluaran</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'expense,asc') fa-sort-up @elseif ($sort == 'expense,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th></th>
      </form>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="8" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @foreach ($items as $item)
          <tr>
            <td class="text-center">{{ ++$iterator }}.</td>
            <td class="text-center">{{ date('d F Y', strtotime($item->transaction_date)) }}</td>
            <td class="text-center">{{ $item->description }}</td>
            <td class="text-center">
              @if ($item->type == 'EXPENSE')
                Pengeluaran
              @else
                Pemasukan
              @endif
            </td>
            <td class="text-right">
              @if ($item->income != 0)
                IDR {{ number_format($item->income, 2, ',', '.') }}
              @endif
            </td>
            <td class="text-right">
              @if ($item->expense != 0)
                IDR {{ number_format($item->expense, 2, ',', '.') }}
              @endif
            </td>
            <td class="text-center">
              <a href="{{ url('cash/'.$item->id.'/edit') }}" class="btn btn-warning" style="color: white">
                <span class="fas fa-edit" title="update"></span>
              </a>
              <button class="btn btn-danger open-confirm" data-name="{{ $item->description }}" data-url="{{ url('cash/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
</div>

<!-- Pagination -->
@component('layouts.components.pagination', [
  'path' => 'cash',
  'items' => $items,
  'token' => $token,
  'show' => $show,
  'search' => $search,
  'sort' => $sort
]) @endcomponent

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop