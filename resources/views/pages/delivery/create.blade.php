@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('delivery-create') }}

<h1 class="text-center">Tambah Surat Jalan</h1>

<br/>

<form method="post" action="{{ url('delivery') }}">
  {{ csrf_field() }}
  <div class="form-group row">
    <label for="customer" class="col-md-4 col-form-label text-md-right">Customer</label>
    <div class="col-md-6">
      <select class="form-control" name="customer" id="customer">
        <option disabled @if (empty(old('customer'))) selected @endif>Pilih Customer</option>
        @foreach ($customers as $item)
          <option value="{{ $item->id }}" @if (old('customer') == $item->id) selected @endif>{{ $item->name }}</option>
        @endforeach
      </select>
      @if ($errors->has('customer'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('customer') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="delivery_number" class="col-md-4 col-form-label text-md-right">Nomor Surat Jalan</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('delivery_number') ? 'is-invalid' : '' }}" name="delivery_number" id="delivery_number" placeholder="Ex : 001/AD/DPO/1/19" value="{{ old('delivery_number') }}">
      @if ($errors->has('delivery_number'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('delivery_number') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label for="delivery_date" class="col-md-4 col-form-label text-md-right">Tanggal</label>
    <div class="col-md-6">
      <input type="date" class="form-control {{ $errors->has('delivery_date') ? 'is-invalid' : '' }}" name="delivery_date" id="delivery_date" placeholder="yyyy-mm-dd" value="{{ old('delivery_date') }}">
      @if ($errors->has('delivery_date'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('delivery_date') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop