@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('delivery-edit', $deliveryOrder) }}

<h1 class="text-center">Ubah Surat Jalan</h1>

<br/>

<form method="post" action="{{ url('delivery/'.$deliveryOrder->id) }}">
  {{ csrf_field() }}
  <input name="_method" type="hidden" value="PATCH">
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Customer</label>
    <div class="col-md-6">
      <select class="form-control" name="customer" id="customer">
        <option disabled @if (empty($deliveryOrder->customer_id)) selected @endif>Pilih Customer</option>
        @foreach ($customers as $item)
          <option value="{{ $item->id }}" @if ($deliveryOrder->customer_id == $item->id) selected @endif>{{ $item->name }}</option>
        @endforeach
      </select>
      @if ($errors->has('customer'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('customer') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Nomor Surat Jalan</label>
    <div class="col-md-6">
      <input type="text" class="form-control {{ $errors->has('delivery_number') ? 'is-invalid' : '' }}"  name="delivery_number" id="delivery_number" value="{{ $deliveryOrder->delivery_number }}">
      @if ($errors->has('delivery_number'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('delivery_number') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Tanggal Order</label>
    <div class="col-md-6">
      <input type="date" class="form-control {{ $errors->has('delivery_date') ? 'is-invalid' : '' }}" name="delivery_date" id="delivery_date" value="{{ $deliveryOrder->delivery_date }}">
      @if ($errors->has('delivery_date'))
        <span class="invalid-feedback">
          <strong>{{ $errors->first('delivery_date') }}</strong>
        </span>
      @endif
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-3 col-form-label"></label>
    <div class="col-sm-9">
      <button type="submit" class="btn btn-secondary">Simpan</button>
    </div>
  </div>
</form>
@stop