<div class="modal fade" id="delivery-line-item-create" tabindex="-1" role="dialog" aria-labelledby="Add Modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" id="form-create">
          {{ csrf_field() }}
          <input type="hidden" name="delivery" value="{{ $deliveryOrder->id }}">
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Nomor Order</b></label>
            <div class="col-sm-9 float-right">
              <select class="form-control" id="order" name="order">
                <option disabled @if (empty(old('order'))) selected @endif>Pilih Order</option>
                @foreach ($orders as $item)
                  <option value="{{ $item->id }}" @if (old('order') == $item->id) selected @endif>{{ $item->order_number }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Produk</b></label>
            <div class="col-sm-9 float-right">
              <select class="form-control" id="product" name="product">
                <option disabled @if (empty(old('product'))) selected @endif>Pilih Produk</option>
                @foreach ($orders as $order)
                  @php $temp = array() @endphp
                  @foreach ($order->lineItems as $item)
                    @if (empty($temp[$item->product->id]))
                      <option value="{{ $item->product->id }}" @if (old('product') == $item->product->id) selected @endif>{{ ucwords(strtolower($item->product->name)) }}</option>
                      @php $temp[$item->product->id] = 1 @endphp
                    @endif
                  @endforeach
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Varian</b></label>
            <div class="col-sm-9 float-right">
              <select class="form-control" id="variant" name="variant">
                <option value="" @if (empty(old('variant'))) selected @endif>Pilih Varian</option>
                @foreach ($orders as $order)
                  @foreach ($order->lineItems as $item)
                    @if (!empty($item->variant))
                      <option value="{{ $item->variant->id }}" @if (old('variant') == $item->variant->id) selected @endif>{{ ucwords(strtolower($item->variant->name)) }}</option>
                    @endif
                  @endforeach
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-3"><b>Jumlah</b></label>
            <div class="col-sm-9 float-right">
              <input type="number" min="1" class="form-control" name="qty" id="qty" value="{{ old('qty') }}">
            </div>
          </div>
          <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>