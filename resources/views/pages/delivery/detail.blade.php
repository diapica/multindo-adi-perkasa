@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('delivery-detail', $deliveryOrder) }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('order'))
  @component('layouts.components.alert', [
    'message' => $errors->first('order'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('product'))
  @component('layouts.components.alert', [
    'message' => $errors->first('product'),
    'type' => 'danger'
  ]) @endcomponent
@endif
@if ($errors->has('qty'))
  @component('layouts.components.alert', [
    'message' => $errors->first('qty'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-6">
    <h3>Detil Surat Jalan</h3>
  </div>
  <div class="col-6">
    <button class="btn btn-outline-secondary open-create float-right" data-url="{{ url('delivery/'.$deliveryOrder->id) }}" data-toggle="modal" data-target="#delivery-line-item-create" style="margin: 0 5px">
      <span class="fas fa-plus text-center">&nbsp Item</span>
    </button>
    <a class="btn btn-outline-secondary float-right" href="{{ url('delivery/'.$deliveryOrder->id.'/edit') }}" style="margin: 0 5px">
      <span class="fas fa-edit text-center">&nbsp Ubah Surat Jalan</span>
    </a>
  </div>
</div>

<!-- Delivery Line Item Create Modal -->
@component('pages.delivery.components.delivery-line-item', [
  'deliveryOrder' => $deliveryOrder,
  'orders' => $orders,
  'products' => $products,
  'variants' => $variants,
]) @endcomponent

<br/>

<div class="row">
  <label class="col-md-3 col-6 col-form-label text-right"><b>Nomor Surat Jalan</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $deliveryOrder->delivery_number }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Tanggal</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ date('d F Y', strtotime($deliveryOrder->delivery_date)) }}</label>
  
  <label class="col-md-3 col-6 col-form-label text-right"><b>Customer</b></label>
  <label class="col-md-9 col-6 col-form-label">{{ $deliveryOrder->customer->name }}</label>
    
  <label class="col-md-3 col-6 col-form-label text-right"><b>Alamat</b></label>
  <label class="col-md-9 col-6 col-form-label">
    @if (!empty($deliveryOrder->address))
      {{ $deliveryOrder->address }}
    @else
      {{ $deliveryOrder->customer->address }}
    @endif
  </label>
</div>

<br/>

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <th class="text-center"><span><b>No.</b></span></th>
      <th class="text-center"><span><b>Produk</b></span></th>
      <th class="text-center"><span><b>Keterangan</b></span></th>
      <th class="text-center" colspan="3"><span><b>Jumlah</b></span></th>
      <th class="text-center"><span><b>Nomor Order</b></span></th>
      <th></th>
    </thead>
    <tbody>
      @if ($deliveryOrder->lineItems->isEmpty())
        <tr>
          <td colspan="8" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @php $i = 0 @endphp
        @foreach ($deliveryOrder->lineItems as $item)
          <tr>
            <td class="text-center">{{ ++$i }}.</td>
            <td>{{ ucwords(strtolower($item->orderLineItem->product->name)) }}</td>
            <td>@if (!empty($item->orderLineItem->variant)) {{ ucwords(strtolower($item->orderLineItem->variant->name)) }} @endif</td>
            <td class="text-center">{{ $item->qty }}</td>
            <td class="text-center">of</td>
            <td class="text-center">{{ $item->orderLineItem->qty }}</td>
            <td>{{ $item->order->order_number }}</td>
            <td class="text-center">
              <button class="btn btn-danger open-confirm" data-name="{{ ucwords(strtolower($item->orderLineItem->product->name)) }}" data-url="{{ url('delivery/'.$deliveryOrder->id.'/item/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
</div>

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop