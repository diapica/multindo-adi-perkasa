@extends('layouts.app')

@section('content')
{{ Breadcrumbs::render('delivery') }}

@if (session('success'))
  @component('layouts.components.alert', [
    'message' => session('success'),
    'type' => 'success'
  ]) @endcomponent
@endif
@if (session('failed'))
  @component('layouts.components.alert', [
    'message' => session('failed'),
    'type' => 'danger'
  ]) @endcomponent
@endif

<div class="row">
  <div class="col-12">
    <h1>Surat Jalan</h1>
  </div>
</div>

<br/>

<!-- Sort & Filter -->
@component('layouts.components.sortfilter', [
  'path' => 'delivery',
  'show' => $show,
  'search' => $search,
  'createPath' => 'delivery/create'
]) @endcomponent

<div class="table-responsive">
  <table class="table table-striped table-bordered" style="width:100%">
    <thead>
      <form method="get" action="{{ url('delivery') }}">
        {{ csrf_field() }}
        <input type="hidden" name="show" id="show" value="{{ $show }}">
        <input type="hidden" name="search" id="search" value="{{ $search }}">
        <input type="hidden" name="sort" id="sort" value="{{ $sort }}">
        <input type="hidden" name="page" id="page" value="{{ $page }}">
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="id">
            <span><b>No.</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline"
            id="delivery_number"
            onclick="setupSort('{{ ($sort == "delivery_number,asc") ? "delivery_number,desc" : "delivery_number,asc" }}')">
            <span><b>Nomor Surat</b></span>
            <span style="padding-top: 4px" class="float-right fa @if ($sort == 'delivery_number,asc') fa-sort-up @elseif ($sort == 'delivery_number,desc') fa-sort-down @else fa-sort @endif"></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="customer">
            <span><b>Customer</b></span>
          </button>
        </th>
        <th class="text-center">
          <button
            class="btn btn-block btn-primary-outline no-click"
            id="delivery_date">
            <span><b>Tanggal</b></span>
          </button>
        </th>
        <th></th>
      </form>
    </thead>
    <tbody>
      @if ($items->isEmpty())
        <tr>
          <td colspan="5" class="text-center">Data tidak ditemukan.</td>
        </tr>
      @else
        @foreach ($items as $item)
          <tr>
            <td class="text-center">{{ ++$iterator }}.</td>
            <td>{{ $item->delivery_number }}</td>
            <td>{{ ucwords(strtolower($item->customer->name)) }}</td>
            <td>{{ date('d F Y', strtotime($item->delivery_date)) }}</td>
            <td class="text-center">
              <a href="{{ url('delivery/'.$item->id) }}" class="btn btn-primary" style="color: white">
                <span class="fas fa-search" title="show"></span>
              </a>
              <button class="btn btn-danger open-confirm" data-name="{{ $item->delivery_number }}" data-url="{{ url('delivery/'.$item->id) }}" data-toggle="modal" data-target="#confirm-delete">
                <span class="fas fa-trash" title="delete"></span>
              </button>
            </td>
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
</div>

<!-- Pagination -->
@component('layouts.components.pagination', [
  'path' => 'delivery',
  'items' => $items,
  'token' => $token,
  'show' => $show,
  'search' => $search,
  'sort' => $sort
]) @endcomponent

<!-- Delete Modal -->
@component('layouts.components.delete') @endcomponent
@stop