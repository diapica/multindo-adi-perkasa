<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />

	<!-- JQuery -->
	<script src="{{ asset('js/jquery-3.3.1.js') }}"></script>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>

	<!-- Font Awesome CSS -->
	<link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">

	<!-- Custom CSS -->
	<style>
		span { font-size: smaller; }
		td { font-size: smaller; }
		.no-click { pointer-events: none; }
		.capitalize { text-transform: capitalize; }
		.uppercase { text-transform: uppercase; }
		.lowercase { text-transform: lowercase; }
		.long { display: none; }
		.btn-primary-outline {
			background-color: transparent;
			border-color: transparent;
		}
	</style>
</head>
<body>
	@include('includes.navbar')

	<div class="container" style="padding-top: 2%">
		@yield('content')
	</div>

	@include('includes.footer')
</body>
</html>
