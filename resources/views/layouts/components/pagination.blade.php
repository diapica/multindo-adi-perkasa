<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3" style="float: left">
  <span>Halaman {{ $items->currentPage() }} dari {{ $items->lastPage() }}</span>
</div>
<div class="col-md-3 col-lg-5" style="float: left; margin: 0"></div>
  <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4" style="float: right">
    <form method="get" action="{{ url($path) }}">
      {{ csrf_field() }}
      <div style="float: right">
        {{ $items->appends([
          '_token' => $token,
          'show' => $show,
          'search' => $search,
          'sort' => $sort
        ])->links("pagination::bootstrap-4") }}
      </div>
    </form>
  </div>
</div>