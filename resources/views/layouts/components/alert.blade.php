<div class="alert
  @if ($type == 'success') alert-success
  @elseif ($type == 'danger') alert-danger @endif
  alert-dismissible fade show">
  {{ $message }}
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>