<form method="get" action="{{ url($path) }}">
  {{ csrf_field() }}
  <div class="form-group col-xs-12 col-sm-12 col-md-4 col-lg-3" style="float: left">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">Show</span>
      </div>
      <select class="custom-select" name="show" id="show" onchange="this.form.submit()">
        <option value="10" {{ ($show == 10 || empty($show)) ? 'selected' : '' }}>10</option>
        <option value="25" {{ ($show == 25) ? 'selected' : '' }}>25</option>
        <option value="50" {{ ($show == 50) ? 'selected' : '' }}>50</option>
        <option value="100" {{ ($show == 100) ? 'selected' : '' }}>100</option>
      </select>
      <div class="input-group-append">
        <span class="input-group-text">entries</span>
      </div>
    </div>
  </div>
  <div class="form-group col-md-3 col-lg-5" style="float: left; margin: 0"></div>
  <div class="form-group col-xs-12 col-sm-12 col-md-5 col-lg-4" style="float: right">
    <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">Search:</span>
      </div>
      <input type="text" class="form-control" name="search" value="{{ $search }}">
      <div class="input-group-append">
        <button class="btn btn-outline-secondary" type="submit">Apply</button>
      </div>
      @if ($createPath)
      <div class="input-group-append">
        <a class="btn btn-outline-secondary" href="{{ url($createPath) }}">
          <span class="fas fa-plus"></span>
        </a>
      </div>
      @endif
    </div>
  </div>
</form>