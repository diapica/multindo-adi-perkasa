<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="Delete Modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Konfirmasi Hapus</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Anda akan menghapus data <b><span id="name"></span></b><br/>Apakah Anda yakin?</p>
      </div>
      <div class="modal-footer">
        <form method="post" id="form-delete">
          {{ csrf_field() }}
          <input name="_method" type="hidden" value="DELETE">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Hapus</button>
        </form>
      </div>
    </div>
  </div>
</div>