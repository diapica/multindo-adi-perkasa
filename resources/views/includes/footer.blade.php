<!-- Confirmation Modal -->
<script>
  $(document).on('click', '.open-confirm', function () {
    var url = $(this).data('url')
    var name = $(this).data('name')
    $('.modal-body #name').text(name)
    $('#form-delete').attr('action', url)
    $('#form-update').attr('action', url)
  })
</script>

<!-- Create Modal -->
<script>
  $(document).on('click', '.open-create', function () {
    var url = $(this).data('url')
    $('#form-create').attr('action', url)
  })
</script>

<!-- Alert JS -->
<!-- <script>
  window.setTimeout(function() {
    $('.alert').fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
    });
  }, 4000);
</script> -->

<!-- Sort JS -->
<script>
  function setupSort(param) { document.getElementById('sort').value = param; }
</script>

<!-- Check All JS -->
<script>
  function checkAll(source, name) {
    checkboxes = document.getElementsByName(name);
    for (var i = 0, n = checkboxes.length; i < n; i++) {
      checkboxes[i].checked = source.checked;
    }
  }
</script>

<!-- CKEditor -->
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script>
  var content = document.getElementById('description');
  CKEDITOR.replace(content, { language:'en-gb' });

  var editor;
  CKEDITOR.on('instanceReady', function(ev) {
    editor = ev.editor;
  });

  function toggleReadOnly(isReadOnly) {
    editor.setReadOnly(isReadOnly);
  }
  CKEDITOR.config.allowedContent = true;
</script>
