<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Laravel') }}</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			@auth
			<li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('/') }}">Beranda</a>
			</li>
			<li class="nav-item dropdown {{ (Request::is('user') || Request::is('user/*')) ? 'active' : '' }}">
				<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
					Manajemen Pengguna<span class="caret"></span>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="{{ url('user/profile') }}">Profil</a>
					{{-- <a class="dropdown-item" href="{{ url('user/permission') }}">Permission</a> --}}
					<a class="dropdown-item" href="{{ url('user/role') }}">Role</a>
					<a class="dropdown-item" href="{{ url('user/member') }}">Member</a>
				</div>
			</li>
			<li class="nav-item {{ (Request::is('customer') || Request::is('customer/*')) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('customer') }}">Customer</a>
			</li>
			<li class="nav-item {{ (Request::is('supplier') || Request::is('supplier/*')) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('supplier') }}">Supplier</a>
			</li>
			<li class="nav-item {{ (Request::is('product') || Request::is('product/*')) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('product') }}">Produk</a>
			</li>
			<li class="nav-item {{ (Request::is('order') || Request::is('order/*')) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('order') }}">Order</a>
			</li>
			<li class="nav-item {{ (Request::is('delivery') || Request::is('delivery/*')) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('delivery') }}">Surat Jalan</a>
			</li>
			<li class="nav-item {{ (Request::is('invoice') || Request::is('invoice/*')) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('invoice') }}">Invoice</a>
			</li>
			<li class="nav-item {{ (Request::is('cash') || Request::is('cash/*')) ? 'active' : '' }}">
				<a class="nav-link" href="{{ url('cash') }}">Kas</a>
			</li>
			<li class="nav-item dropdown {{ (Request::is('report') || Request::is('report/*')) ? 'active' : '' }}">
				<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
					Laporan<span class="caret"></span>
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="{{ url('report/order-in') }}">Order Masuk</a>		
					<a class="dropdown-item" href="{{ url('report/order-out') }}">Order Keluar</a>	
					<a class="dropdown-item" href="{{ url('report/cash') }}">Kas</a>
					<a class="dropdown-item" href="{{ url('report/delivery') }}">Surat Jalan</a>
					<a class="dropdown-item" href="{{ url('report/profit') }}">Keuntungan</a>		
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					Logout
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
					{{ csrf_field() }}
				</form>
			</li>
			@endauth
			
			@guest
			<li class="nav-item {{ Request::is('login') ? 'active' : '' }}">
				<a class="nav-link" href="{{ route('login') }}">Login</a>
			</li>
			@endguest
		</ul>
	</div>
</nav>